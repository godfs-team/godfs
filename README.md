# GODFS project README
#### 19 Nov 2019

Our code is located at [Gitlab](https://gitlab.com/godfs-team/godfs)

Images for docker are effectively the only one image with different entry points:
 * /app/namenode/build - to run the namenode
 * /app/datanode/build - to run a datanode
 * /app/client/build   - to run a client

Image for **deployment** on docker swarm is available at  
registry.gitlab.com/godfs-team/godfs/base:swarm

If you want to start our system by yourself, you may build docker image and run docker compose:
 * run  ```./docker/base/build-with-cache.sh``` from the project root to build :local image of system
 * run ```docker-compose up``` to start nodes
 * run client with provided environment variable ```NAMENODE_ADDRESS``` with value ```<your_host_machine_ip>:9090```. Using docker image you should first execute interactive shell to deal with this step or just run   ```docker run -e NAMENODE_ADDRESS=<your_host_machine_ip>:9090 -it registry.gitlab.com/godfs-team/godfs/base:local /app/client/build```
 * list available commands in client via typing command ```help```, they are pretty much like the ones from linux shell

## Task + Requirements
- Task: 
    - Design and implement **Distributed File System**
- Requirements: 
    - File reading, writing, creation, deletion, copy, moving and info queries
    - Directory operations - listing, creation, changing and deletion
    - Transparent on-fly replication to multiple storage servers
    - Fault Tolerance
    - Loading file contents from and into DFS

## Team + Contributions
- **Ilya**
    - Implementation (the biggest part)
    - System logic
    - Architecture
    - API
    - Report
- **Kamil**
    - Deployment
    - Architecture
    - API
    - Implementation
    - Management
    - Report
- **Kamilla**
    - Presentation
    - Visuals
    - Report
    - Implementation

## Development Stack
- Go
- Redis
- gRPC + ProtoBuf
- Docker + Swarm
- Gitlab CI/CD
- AWS EC2

## Architecture
![](https://i.imgur.com/DXL2tmC.png)


## Project Structure
![](https://i.imgur.com/CBHiLMH.png)


## API
- Namenode public API
    - Init
    - ChangeNamespaceEntity
    - NamespaceEntityInfo
- Namenode private API
    - RegisterDatanode
    - UnregisterDatanode
    - GetFileInfo
- Datanode public API
    - Read
    - Write
- Datanode private API
    - Ping
    - Create
    - Delete
    - Read
    - Write
    - Replicate
    - InitStorage

## Implementation
- We use transparent on-fly **replication** for any write operation over file, so for client it looks like he writes file only to one datanode, while this datanode, using private network, communicates with other ```MinReplicaCount-1``` datanodes to replicate changes to this file simultaneously with client write operation
- Same direct replication mechanism is used for copy replication, datanodes receives replication tasks and execute them

## Deployment
- Docker + Swarm
- Gitlab CI
- AWS EC2 instances

## Contacts
- Telegram:
    - Ilya @ilya_potemin
    - Kamil @kamilkoduo
    - Kamilla @camillakrr


## Useful stuff
### Clone repo as package using SSH:
* `git config --global --add url."git@gitlab.com:".insteadOf "https://gitlab.com/"`
* `go get -v gitlab.com/godfs-team/godfs`