#!/usr/bin/env bash

build(){
local COMPONENT=client
local TAG=local
local DOCKER_REGISTRY=registry.gitlab.com/godfs-team/godfs/${COMPONENT}
local DOCKER_PATH=docker/${COMPONENT}/Dockerfile
local CONTEXT=.

docker build -t ${DOCKER_REGISTRY}:${TAG} -f ${DOCKER_PATH} ${CONTEXT} --no-cache
}
build
