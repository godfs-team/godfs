#!/usr/bin/env bash

build(){
local COMPONENT=base
local TAG=stable
local DOCKER_REGISTRY=registry.gitlab.com/godfs-team/godfs/${COMPONENT}

docker push ${DOCKER_REGISTRY}:${TAG}
}
build
