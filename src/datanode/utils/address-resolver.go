package utils

import (
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
)

func GetPortString(address string) (port string) {
	if strings.Contains(address, ":") {
		colonInd := strings.LastIndex(address, ":")
		return address[colonInd+1:]
	} else {
		return "port"
	}
}

func ProcessAddressTemplate(address string) (addr string, err error) {
	if strings.Contains(address, "<public>") {
		ipStr, err := getMachinePublicIP()
		if err != nil {
			return "", err
		}
		return strings.ReplaceAll(address, "<public>", ipStr), nil
	}
	return address, nil
}

func getMachinePublicIP() (str string, err error) {
	request, err := http.NewRequest(http.MethodGet, "https://2ip.ru", nil)
	if err != nil {
		return "", err
	}
	request.Header.Set("User-Agent", "curl/7.64.0")
	request.Header.Set("Accept", "*/*")
	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return "", errors.New("unable to connect to server to retrieve node public address:" + err.Error())
	}
	defer func() {
		_ = response.Body.Close()
	}()
	ip, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return "", errors.New("unable to read server answer to retrieve node public address:" + err.Error())
	}
	return string(ip[:len(ip)-1]), nil
}
