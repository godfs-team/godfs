package main

import (
	"fmt"
	grpc_server "gitlab.com/godfs-team/godfs/src/datanode/grpc-server"
	"google.golang.org/grpc"
	"log"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	fmt.Println("Loading up datanode servers")
	runPublicServer := func() {
		s := grpc_server.PublicGRPCServer{}
		srv := grpc.NewServer()
		err := s.RunServer(srv)
		if err != nil {
			log.Fatalln("Public server run error: ", err)
		}
	}
	runPrivateServer := func() {
		s := grpc_server.PrivateGRPCServer{}
		srv := grpc.NewServer()
		err := s.RunServer(srv)
		if err != nil {
			log.Fatalln("Private server run error: ", err)
		}
	}
	go listenShutdownHook()
	go runPrivateServer()
	runPublicServer()
}

func listenShutdownHook() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		select {
		case _ = <-c:
			{

			}
		}
	}()
}
