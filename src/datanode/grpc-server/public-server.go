package grpc_server

import (
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/godfs-team/godfs/src/datanode/api"
	"gitlab.com/godfs-team/godfs/src/datanode/service"
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	"google.golang.org/grpc"
	"net"
)

// PublicGRPCServer --
type PublicGRPCServer struct{}

func (s *PublicGRPCServer) RunServer(srv *grpc.Server) (err error) {
	api.RegisterDatanodeServer(srv, s)

	var listenAddress = service.PublicAddress
	listenAddress = "0.0.0.0:" + utils.GetPortString(listenAddress)
	l, err := net.Listen("tcp", listenAddress)
	if err != nil {
		return err
	}
	err = srv.Serve(l)
	if err != nil {
		return err
	}
	return nil
}

func (s *PublicGRPCServer) Write(requestStream api.Datanode_WriteServer) error {
	stat, err := service.Write(requestStream, true)
	if err != nil {
		fmt.Println("REQ Write exited with error: ", err)
		err = requestStream.SendAndClose(&api.DatanodeResponse{
			Status: api.DatanodeStatus(stat),
			Msg:    err.Error(),
		})
	} else {
		fmt.Println("REQ Write executed")
	}
	return nil
}

func (s *PublicGRPCServer) Read(request *api.ReadRequest, responseStream api.Datanode_ReadServer) error {
	fileUuid, err := uuid.Parse(request.UUID)
	if err != nil {
		fmt.Println("REQ Read failed (parsing): ", err)
		err = responseStream.Send(&api.ReadResponse{
			Response: &api.DatanodeResponse{
				Status: api.DatanodeStatus_ERROR,
				Msg:    "unable to parse file uuid: " + err.Error(),
			},
			Chunk: nil,
		})
	}
	err = service.Read(fileUuid, request.Offset, request.MaxSize, responseStream)
	if err != nil {
		fmt.Println("REQ Read failed: ", err)
		err = responseStream.Send(&api.ReadResponse{
			Response: &api.DatanodeResponse{
				Status: api.DatanodeStatus_ERROR,
				Msg:    err.Error(),
			},
			Chunk: nil,
		})
	} else {
		fmt.Println("REQ Read executed")
	}
	return nil
}
