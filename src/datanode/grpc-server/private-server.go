package grpc_server

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/godfs-team/godfs/src/datanode/api"
	"gitlab.com/godfs-team/godfs/src/datanode/service"
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	"google.golang.org/grpc"
	"log"
	"net"
)

type PrivateGRPCServer struct {
	publicServer PublicGRPCServer
}

func (s *PrivateGRPCServer) Ping(context.Context, *api.InitRequest) (*api.DatanodeResponse, error) {
	var answerMsg string
	if service.DatanodeUuid == uuid.Nil {
		answerMsg = "no uuid"
	} else {
		answerMsg = service.DatanodeUuid.String()
	}
	return &api.DatanodeResponse{
		Status: api.DatanodeStatus_OK,
		Msg:    answerMsg,
	}, nil
}

func (s *PrivateGRPCServer) Read(request *api.ReadRequest, readServer api.DatanodePrivate_ReadServer) error {
	return s.publicServer.Read(request, readServer)
}

func (s *PrivateGRPCServer) Write(writeServer api.DatanodePrivate_WriteServer) error {
	stat, err := service.Write(writeServer, false)
	if err != nil {
		fmt.Println("REQ Write[PRIV] exited with error: ", err)
		err = writeServer.SendAndClose(&api.DatanodeResponse{
			Status: api.DatanodeStatus(stat),
			Msg:    err.Error(),
		})
	} else {
		fmt.Println("REQ Write[PRIV] executed")
	}
	return nil
}

func (s *PrivateGRPCServer) Create(ctx context.Context, request *api.CreateRequest) (response *api.DatanodeResponse, err error) {
	fileUuid, err := uuid.Parse(request.UUID)
	if err != nil {
		fmt.Println("REQ Create failed (parsing): ", err)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_ERROR,
			Msg:    "unable to parse file uuid: " + err.Error(),
		}, nil
	}
	err = service.Create(fileUuid)
	if err != nil {
		fmt.Println("REQ Create failed: ", err)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_ERROR,
			Msg:    err.Error(),
		}, nil
	} else {
		fmt.Println("REQ Create executed: FILE ", request.UUID)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_OK,
			Msg:    "ok",
		}, nil
	}
}

func (s *PrivateGRPCServer) Delete(ctx context.Context, request *api.DeleteRequest) (response *api.DatanodeResponse, err error) {
	fileUuid, err := uuid.Parse(request.UUID)
	if err != nil {
		fmt.Println("REQ Delete failed (parsing): ", err)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_ERROR,
			Msg:    "unable to parse file uuid: " + err.Error(),
		}, nil
	}
	stat, err := service.Delete(fileUuid)
	if err != nil {
		fmt.Println("REQ Delete failed: ", err)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus(stat),
			Msg:    err.Error(),
		}, nil
	} else {
		fmt.Println("REQ Delete executed: ", request.UUID)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus(stat),
			Msg:    "ok",
		}, nil
	}
}

func (s *PrivateGRPCServer) Replicate(ctx context.Context, replTask *api.ReplicationTask) (*api.DatanodeResponse, error) {
	//fmt.Println("REQ Replicate no implemented: ", replTask.TaskUuid)
	//panic("implement me")
	err := service.BeginReplication(replTask.Source, replTask.Target)
	if err != nil {
		log.Println("REQ Replicate error: ", err)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_ERROR,
			Msg:    err.Error(),
		}, nil
	} else {
		fmt.Println("REQ Replicate task executed: ", replTask.TaskUuid)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_OK,
			Msg:    "ok",
		}, nil
	}
}

func (s *PrivateGRPCServer) InitStorage(ctx context.Context, request *api.InitRequest) (*api.DatanodeResponse, error) {
	err := service.InitStorage()
	if err != nil {
		log.Printf("InitStorage error: %v\n", err)
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_ERROR,
			Msg:    err.Error(),
		}, nil
	} else {
		log.Printf("InitStorage successfully executed\n")
		return &api.DatanodeResponse{
			Status: api.DatanodeStatus_OK,
			Msg:    "ok",
		}, nil
	}
}

func (s *PrivateGRPCServer) RunServer(srv *grpc.Server) (err error) {
	api.RegisterDatanodePrivateServer(srv, s)
	listenAddress := "0.0.0.0:" + utils.GetPortString(service.PrivateAddress)
	l, err := net.Listen("tcp", listenAddress)
	if err != nil {
		return err
	}
	err = srv.Serve(l)
	if err != nil {
		return err
	}
	return nil
}
