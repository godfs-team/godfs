package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/godfs-team/godfs/src/datanode/api"
	nnapi "gitlab.com/godfs-team/godfs/src/namenode/api"
	"google.golang.org/grpc"
	"io"
	"math"
	"os"
	"strconv"
)

// Read stream till the end, write chunks and return status/error
func Write(requestStream api.Datanode_WriteServer, withReplication bool) (status int, msg error) {

	// TODO: pull file info by uuid from nameserver
	// TODO: open connections to all storing dataservers
	var fileDatanodes []*nnapi.DataNodeInfo
	var file *os.File
	var datanodeWrites []api.Datanode_WriteClient
	defer func() {
		if file != nil {
			_ = file.Close()
		}
	}()
	for {
		writeRequest, err := requestStream.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}

			return 1, err
		}
		if file == nil {
			fileName := StorageDir + "/" + writeRequest.UUID
			file, err = os.OpenFile(fileName, os.O_WRONLY, 0666)
			if err != nil {
				return 2, err
			}
			if withReplication {
				nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
				if err != nil {
					return 10, errors.New("datanode connection error: " + err.Error())
				}
				nnClient := nnapi.NewNamenodePrivateClient(nnConn)
				fileInfoRequest := &nnapi.FileInfoRequest{
					FileUuid: writeRequest.UUID,
				}
				response, err := nnClient.GetFileInfo(context.TODO(), fileInfoRequest)
				if err != nil {
					return 4, err
				}
				fileDatanodes = response.Info.DataNodes
				datanodeWrites = make([]api.Datanode_WriteClient, len(fileDatanodes))
				for i := 0; i < len(fileDatanodes); i++ {
					// it is private address now
					datanodePublicAddress := fileDatanodes[i].ServerAddress
					dnConn, err := grpc.Dial(datanodePublicAddress, grpc.WithInsecure())
					if err != nil {
						fmt.Println("Unable to connect to the datanode " + fileDatanodes[i].ServerUuid + " at " + datanodePublicAddress + ": " + err.Error())
					}
					datanodeWrites[i], err = api.NewDatanodePrivateClient(dnConn).Write(context.TODO())
					if err != nil {
						fmt.Println("Unable to open write connection to the datanode at address", datanodePublicAddress, " to perform transparent write replication:", err)
					}
				}
			}
		}
		if writeRequest.Offset != 0 {
			_, err = file.Seek(writeRequest.Offset, 0)
			if err != nil {
				return 3, err
			}
		}
		// now everything is correct, prepare to the transparent replication

		_, err = file.Write(writeRequest.Chunk)
		if err != nil {
			return 4, err
		}
		if withReplication {
			for i := 0; i < len(datanodeWrites); i++ {
				request := &api.WriteRequest{
					UUID:   writeRequest.UUID,
					Offset: writeRequest.Offset,
					Chunk:  writeRequest.Chunk,
				}
				err = datanodeWrites[i].Send(request)
				if err != nil {
					fmt.Println("Unable to execute transparent replication: datanode write error: " + err.Error())
				}
			}
		}
	}
	_ = requestStream.SendAndClose(&api.DatanodeResponse{
		Status: api.DatanodeStatus_OK,
		Msg:    "success writing",
	})
	if datanodeWrites != nil {
		for i := 0; i < len(datanodeWrites); i++ {
			_, err := datanodeWrites[i].CloseAndRecv()
			if err != nil {
				fmt.Println("Replication finalizaion failed: " + err.Error())
			}
		}
	}
	return 0, nil
}

// Read provided file info and write chunks into responseStream
func Read(fileUuid uuid.UUID, offset int64, maxSize int64, responseStream api.Datanode_ReadServer) (err error) {
	if maxSize == -1 {
		maxSize = math.MaxInt64
	}
	if maxSize < 0 {
		// nothing should be read
		return errors.New("negative max size")
	}
	if offset > maxSize {
		// nothing could be read
		return nil
	}

	// obtain file and its info
	file, err := os.OpenFile(StorageDir+"/"+fileUuid.String(), os.O_RDONLY, 0444)
	defer func() {
		if file != nil {
			_ = file.Close()
		}
	}()
	if err != nil {
		return err
	}
	fileInfo, err := file.Stat()
	if err != nil {
		return err
	}

	// how much should or can we read from this file?-
	readSizeLeft := int64(math.Round(math.Min(float64(fileInfo.Size()), float64(maxSize))))

	// does something left to read?
	if offset > readSizeLeft {
		// there is nothing left to read
		return nil
	} else {
		readSizeLeft -= offset
	}

	// prepare to read
	_, err = file.Seek(offset, io.SeekStart)
	if err != nil {
		return err
	}

	// send chunk by chunk
	for readSizeLeft > 0 {
		currentChunkSize := int64(math.Round(math.Min(float64(readSizeLeft), float64(ReadChunkSize))))
		buffer := make([]byte, currentChunkSize)
		n, err := file.Read(buffer)
		// check for file reading error
		if err != nil {
			return err
		}
		if int64(n) != currentChunkSize {
			aStr := strconv.Itoa(int(currentChunkSize))
			bStr := strconv.Itoa(n)
			return errors.New("mismatched read chunk length: expected " + aStr + ", got " + bStr)
		}
		// send chunk
		err = responseStream.Send(&api.ReadResponse{
			Response: &api.DatanodeResponse{
				Status: api.DatanodeStatus_OK,
				Msg:    "",
			},
			Chunk: buffer,
		})
		if err != nil {
			return err
		}
		readSizeLeft -= currentChunkSize
	}
	return nil
}
