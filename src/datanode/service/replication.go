package service

import (
	"context"
	"errors"
	"gitlab.com/godfs-team/godfs/src/datanode/api"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
)

func BeginReplication(source *api.ReplicationBlock, target *api.ReplicationBlock) (err error) {
	targetConn, err := grpc.Dial(target.DatanodeAddress, grpc.WithInsecure())
	if err != nil {
		log.Println("Unable to connect to the target endpoint", target.DatanodeAddress, "to perform replication task:", err)
		return err
	}
	targetPublicClient := api.NewDatanodePrivateClient(targetConn)
	writeServer, err := targetPublicClient.Write(context.TODO())
	if err != nil {
		log.Println("Unable to open write connection to the target endpoint", target.DatanodeAddress, ": ", err)
		return err
	}
	file, err := os.OpenFile(StorageDir+"/"+source.DataUuid, os.O_RDONLY, 0666)
	if err != nil {
		log.Println("Unable to open FILE ", source.DataUuid, " on current machine")
		return err
	}
	var readSize int64
	if source.DataEndByte != -1 {
		readSize = source.DataEndByte - source.DataStartByte
	} else {
		fileInfo, _ := file.Stat()
		readSize = fileInfo.Size() - source.DataStartByte
	}
	_, _ = file.Seek(source.DataStartByte, io.SeekStart)
	buffer := make([]byte, readSize)
	_, _ = file.Read(buffer)
	err = writeServer.Send(&api.WriteRequest{
		UUID:   target.DataUuid,
		Offset: target.DataStartByte,
		Chunk:  buffer,
	})
	if err != nil {
		log.Println("Unable to send replication file chunk: ", err)
		return err
	}
	response, err := writeServer.CloseAndRecv()
	if err != nil {
		log.Println("Error while finalizing replication task: ", err)
		return err
	}
	if response.Status != api.DatanodeStatus_OK {
		log.Println("Replication finalization returned not-ok status: ", response.Msg)
		return errors.New(response.Msg)
	}
	return nil
}
