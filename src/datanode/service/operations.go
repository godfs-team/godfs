package service

import (
	"github.com/google/uuid"
	"gitlab.com/godfs-team/godfs/src/datanode/api"
	"os"
)

// Create file named StorageDir/fileUuid
func Create(fileUuid uuid.UUID) (err error) {
	fullFileName := StorageDir + "/" + fileUuid.String()
	if _, err := os.Stat(fullFileName); err == nil {
		// file already exists
		return err
	} else {
		// file does not exists, try to create it
		_, err := os.Create(fullFileName)
		if err != nil {
			// creation failed, report it
			return err
		} else {
			// success
			return nil
		}
	}

}

//Delete file named StorageDir/fileUuid
func Delete(fileUuid uuid.UUID) (status int, msg error) {
	fullFileName := StorageDir + "/" + fileUuid.String()
	if _, err := os.Stat(fullFileName); err == nil {
		// file exists, try to remove it
		err = os.Remove(fullFileName)
		if err != nil {
			// deletion failed
			return int(api.DatanodeStatus_ERROR), err
		} else {
			return int(api.DatanodeStatus_OK), nil
		}
	} else {
		// file is not found
		return int(api.DatanodeStatus_ERROR), err
	}
}
