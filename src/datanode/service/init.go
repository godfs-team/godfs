package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	nnapi "gitlab.com/godfs-team/godfs/src/namenode/api"
	"google.golang.org/grpc"
	"log"
	"net"
	"os"
)

var DatanodeUuid uuid.UUID

func init() {
	fmt.Println("Loading up datanode services")
	fmt.Println("Active variables:")
	fmt.Println("  STORAGE_DIR      = ", StorageDir)
	fmt.Println("  READ_CHUCK_SIZE  = ", ReadChunkSize)
	fmt.Println("  PUBLIC_ADDRESS   = ", PublicAddress)
	fmt.Println("  PRIVATE_ADDRESS  = ", PrivateAddress)
	fmt.Println("  NAMENODE_ADDRESS = ", NamenodeAddress)
	generatedUuid, realPrivateAddress, err := registerOnNamenode()
	if err != nil {
		fmt.Println("Unable to register on the namenode: ", err.Error())
		os.Exit(1)
	} else {
		fmt.Println("Datanode given uuid = ", generatedUuid.String())
		if realPrivateAddress != PrivateAddress {
			fmt.Println("Real private address of datanode is " + realPrivateAddress + ", updated")
			PrivateAddress = realPrivateAddress
		}
	}
	err = ensureStorageCreated()
	if err != nil {
		fmt.Println("Unable to initialize locat storage: ", err.Error())
		os.Exit(2)
	} else {
		fmt.Println("Local storage initialized")
	}
	DatanodeUuid = generatedUuid
}

func registerOnNamenode() (generatedUuid uuid.UUID, realAddress string, err error) {
	address := NamenodeAddress
	datanodePrivateAddress := PrivateAddress
	datanodePublicAddress := PublicAddress
	nnConn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		return uuid.UUID{}, "", err
	}
	defer func() {
		_ = nnConn.Close()
	}()
	getAddrFunc := func() string {
		conn, err := net.Dial("tcp", address)
		if err != nil {
			log.Println("Unable to connect to namenode as plain tcp socker")
			return datanodePrivateAddress
		}
		defer conn.Close()
		localAddr := conn.LocalAddr().(*net.TCPAddr)
		return localAddr.IP.String() + ":" + utils.GetPortString(datanodePrivateAddress)
	}
	datanodePrivateAddress = getAddrFunc()
	nnPrivateClient := nnapi.NewNamenodePrivateClient(nnConn)
	registerRequest := &nnapi.DatanodeRegisterRequest{
		PrivateAddress: datanodePrivateAddress,
		PublicAddress:  datanodePublicAddress,
	}
	response, err := nnPrivateClient.RegisterDatanode(context.TODO(), registerRequest)
	if err != nil {
		return uuid.UUID{}, "", errors.New("error while registration: " + err.Error())
	}
	if response.Status != nnapi.Status_OK {
		return uuid.UUID{}, "", errors.New("namenode returned not-ok status: " + response.Msg)
	}
	generatedUuid, err = uuid.Parse(response.GeneratedUuid)
	if err != nil {
		return uuid.UUID{}, "", errors.New("namenode returned invalid generated uuid: " + response.GeneratedUuid)
	}
	return generatedUuid, response.PrivateAddress, nil
}

func UnregisterFromNamenode() (err error) {
	//address := NamenodeAddress
	//nnConn, err := grpc.Dial(address, grpc.WithInsecure())
	//if err != nil {
	//	return uuid.UUID{}, err
	//}
	//defer func() {
	//	_ = nnConn.Close()
	//}()
	//nnPrivateClient := nnapi.NewNamenodePrivateClient(nnConn)
	return nil
}

func ensureStorageCreated() (err error) {
	if _, err := os.Stat(StorageDir); os.IsNotExist(err) {
		err = os.MkdirAll(StorageDir, 0777)
		if err != nil {
			return err
		} else {
			log.Println("Storage dir ", StorageDir, " was created clean")
		}
	} else {
		err = os.RemoveAll(StorageDir)
		if err != nil {
			log.Println("Unable to remove", StorageDir, " for fresh initialization:", err)
			return err
		}
		err = os.MkdirAll(StorageDir, 0777)
		if err != nil {
			return err
		} else {
			log.Println("Storage dir ", StorageDir, " was not empty, cleaned up")
		}
	}
	return nil
}

func InitStorage() error {
	err := ensureStorageCreated()
	return err
}
