package service

import (
	"fmt"
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	"log"
	"os"
	"strconv"
)

/* vars */

/* consts */

const defaultStorageDir = "/storage"

const defaultPublicAddress = "0.0.0.0:8080"
const defaultPrivateAddress = "0.0.0.0:8081"
const defaultNamenodeAddress = "namenode:9091"


var PublicAddress = func() string {
	var err error
	addr, passed := os.LookupEnv("PUBLIC_ADDRESS")
	if !passed {
		addr = defaultPublicAddress
	} else {
		addr, err = utils.ProcessAddressTemplate(addr)
		if err != nil {
			log.Fatalln("Unable to retrieve public address of the node: ", err)
		}
	}
	return addr
}()

var PrivateAddress = func() string {
	addr, passed := os.LookupEnv("PRIVATE_ADDRESS")
	if !passed {
		addr = defaultPrivateAddress
	}
	return addr
}()

var NamenodeAddress = func() string {
	addr, passed := os.LookupEnv("NAMENODE_ADDRESS")
	if !passed {
		addr = defaultNamenodeAddress
	}
	return addr
}()

var StorageDir = func() string {
	storage, found := os.LookupEnv("STORAGE_DIR")
	if !found {
		storage = defaultStorageDir
	}
	return storage
}()

var ReadChunkSize = func() int {
	if readChunkSize, found := os.LookupEnv("READ_CHUNK_SIZE"); found {
		size, err := strconv.Atoi(readChunkSize)
		if err != nil {
			fmt.Printf("unable to parse provided environment value READ_CHUNK_SIZE: " + err.Error())
			return 4096
		}
		return size
	} else {
		return 4096
	}
}()
