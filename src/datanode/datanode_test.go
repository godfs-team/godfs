//// +build test

package datanode_test

//import (
//	"context"
//	"github.com/google/uuid"
//	. "github.com/onsi/ginkgo"
//	. "github.com/onsi/gomega"
//	"gitlab.com/godfs-team/godfs/src/datanode/api"
//	grpc_server "gitlab.com/godfs-team/godfs/src/datanode/grpc-server"
//	"google.golang.org/grpc"
//	"log"
//	"time"
//)
//
//var _ = Describe("Datanode", func() {
//	var (
//		service struct {
//			server           *grpc.Server
//			client           api.DatanodeClient
//			clientConnection *grpc.ClientConn
//			ctx              context.Context
//			ctxCancel        context.CancelFunc
//		}
//		testFileUUID uuid.UUID
//	)
//	BeforeSuite(func() {
//		//run server
//		log.Print("Step1")
//		go func() {
//			s := grpc_server.PublicGRPCServer{}
//			service.server = grpc.NewServer()
//			s.RunServer(service.server)
//			if service.server == nil {
//				log.Fatal("NIL")
//			}
//		}()
//
//		//setup client connection
//		func() {
//			conn, err := grpc.Dial(grpc_server.PublicAddress, grpc.WithInsecure())
//			if err != nil {
//				log.Fatalf("Could not connect: %v", err)
//			}
//			service.client = api.NewDatanodeClient(conn)
//			service.clientConnection = conn
//		}()
//		//setup context
//		func() {
//			service.ctx, service.ctxCancel = context.WithTimeout(context.Background(), time.Second)
//		}()
//
//		//setup test helpers
//		func() {
//			testFileUUID = uuid.New()
//		}()
//
//	})
//	/*
//	Тут иногда вылетает тест, нуллпоинтер почему то
//	AfterSuite(func() {
//		// run server
//		func() {
//			service.server.GracefulStop()
//		}()
//		// release client connection
//		func() {
//			_ = service.clientConnection.Close()
//		}()
//		// release context
//		func() {
//			service.ctxCancel()
//		}()
//	})
//	*/
//	Describe("Interacting with Datanode GRPC API", func() {
//		Context("sending a create request", func() {
//			PIt("should create the file", func() {
//				createRequest := api.CreateRequest{UUID: testFileUUID.String()}
//				response, err := service.client.Create(service.ctx, &createRequest)
//				Expect(err).ShouldNot(HaveOccurred())
//				Expect(response.Status).To(Equal(api.Status_OK))
//			})
//		})
//		Context("sending a delete request", func() {
//			PIt("should delete the file", func() {
//				createRequest := api.CreateRequest{UUID: testFileUUID.String()}
//				Expect(service.client.Create(service.ctx, &createRequest)).Should(Succeed())
//
//				deleteRequest := api.DeleteRequest{UUID: testFileUUID.String()}
//				response, err := service.client.Delete(service.ctx, &deleteRequest)
//				Expect(err).ShouldNot(HaveOccurred())
//				Expect(response.Status).To(Equal(api.Status_OK))
//			})
//		})
//	})
//})
