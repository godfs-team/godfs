package shell

import (
	"bufio"
	"context"
	"errors"
	"fmt"
	"gitlab.com/godfs-team/godfs/src/client/api"
	"log"
	"os"
	"strings"
)

type ClientState struct {
	CurrentLocalFolder  string
	CurrentRemoteFolder string
}

func getApi(ctx context.Context) api.ClientApi {
	return ctx.Value("api").(api.ClientApi)
}

func getCurrentLocalDir(ctx context.Context) string {
	return ctx.Value("state").(*ClientState).CurrentLocalFolder
}

func getCurrentRemoteDir(ctx context.Context) string {
	return ctx.Value("state").(*ClientState).CurrentRemoteFolder
}

func setCurrentLocalDir(ctx context.Context, fullLocalDirPath string) {
	ctx.Value("state").(*ClientState).CurrentLocalFolder = fullLocalDirPath

}

func setCurrentRemoteDir(ctx context.Context, fullRemoteDirPath string) {
	ctx.Value("state").(*ClientState).CurrentRemoteFolder = fullRemoteDirPath
}

func expandLocalPath(ctx context.Context, localPath string) string {
	if localPath == "." {
		return getCurrentLocalDir(ctx)
	}
	if localPath == ".." {
		lastSlash := strings.LastIndex(getCurrentLocalDir(ctx), "/")
		return getCurrentLocalDir(ctx)[:lastSlash]
	}
	if !strings.HasPrefix(localPath, "/") {
		return getCurrentLocalDir(ctx) + "/" + localPath
	}
	return localPath
}

func expandRemotePath(ctx context.Context, remotePath string) string {
	if remotePath == "." {
		return getCurrentRemoteDir(ctx)
	}
	if remotePath == ".." {
		lastSlash := strings.LastIndex(getCurrentRemoteDir(ctx), "/")
		return getCurrentRemoteDir(ctx)[:lastSlash]
	}
	if !strings.HasPrefix(remotePath, "/") {
		return getCurrentRemoteDir(ctx) + "/" + remotePath
	}
	return remotePath
}

// init
func DoInitStorage(ctx context.Context, args []string) error {
	st, msg := getApi(ctx).InitStorage()
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// touch <remote file name>
func DoCreateFile(ctx context.Context, args []string) error {
	if len(args) != 1 {
		return errors.New("create requires exactly 1 argument")
	}
	remoteFilePath := expandRemotePath(ctx, args[0])
	st, msg := getApi(ctx).CreateFile(remoteFilePath)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// read <remote file name> <local file name>
func DoReadFile(ctx context.Context, args []string) error {
	if len(args) != 2 {
		return errors.New("read requires exactly 2 arguments")
	}
	remoteFilePath := expandRemotePath(ctx, args[0])
	localFilePath := expandLocalPath(ctx, args[1])
	st, msg := getApi(ctx).ReadFile(remoteFilePath, localFilePath)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// write <local file name> <remote file name>
func DoWriteFile(ctx context.Context, args []string) error {
	if len(args) != 2 {
		return errors.New("write requires exactly 2 arguments")
	}
	localFilePath := expandLocalPath(ctx, args[0])
	remoteFilePath := expandRemotePath(ctx, args[1])
	st, msg := getApi(ctx).WriteFile(localFilePath, remoteFilePath)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// rm <remote file name>
func DoRemoveFile(ctx context.Context, args []string) error {
	if len(args) != 1 {
		return errors.New("remove requires exactly 1 argument")
	}
	remoteFilePath := expandRemotePath(ctx, args[0])
	st, msg := getApi(ctx).RemoveFile(remoteFilePath)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// info <remote file name>
func GetFileInfo(ctx context.Context, args []string) error {
	if len(args) != 1 {
		return errors.New("info requires exactly 1 argument")
	}
	remoteFilePath := expandRemotePath(ctx, args[0])
	st, msg, info := getApi(ctx).GetFileInfo(remoteFilePath)
	if st == api.Status_ERROR {
		log.Println(msg)
	} else {
		fmt.Println("FILE " + remoteFilePath + ", uuid = " + info.FileUUID)
		fmt.Println("Stored on:")
		for i := range info.DataNodes {
			fmt.Println("  datanode " + info.DataNodes[i].ServerUuid + " with public address " + info.DataNodes[i].ServerAddress)
		}
	}
	return nil

}

// cp <remote file name> <remote file name>
func DoCopyFile(ctx context.Context, args []string) error {
	if len(args) != 2 {
		return errors.New("copy requires exactly 2 arguments")
	}
	remoteFilePathSource := expandRemotePath(ctx, args[0])
	remoteFilePathTarget := expandRemotePath(ctx, args[1])
	st, msg := getApi(ctx).CopyFile(remoteFilePathSource, remoteFilePathTarget)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// mv <remote file name> <remote file name>
func DoMoveFile(ctx context.Context, args []string) error {
	if len(args) != 2 {
		return errors.New("move requires exactly 2 arguments")
	}
	remoteFilePathSource := expandRemotePath(ctx, args[0])
	remoteFilePathTarget := expandRemotePath(ctx, args[1])
	st, msg := getApi(ctx).MoveFile(remoteFilePathSource, remoteFilePathTarget)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// cd <remote dir name>
func DoChangeDir(ctx context.Context, args []string) error {
	if len(args) != 1 {
		return errors.New("change dir requires exactly 1 argument")
	}
	newFullDirPath := expandRemotePath(ctx, args[0])
	st, msg := getApi(ctx).ChangeDirRemote(newFullDirPath)
	if st == api.Status_ERROR {
		log.Println(msg)
	} else {
		setCurrentRemoteDir(ctx, newFullDirPath)
	}
	return nil
}

// ls [remote dir name]
func DoListDir(ctx context.Context, args []string) error {
	var fullDirPath string
	if len(args) == 0 {
		fullDirPath = "."
	} else if len(args) == 1 {
		fullDirPath = args[0]
	} else {
		return errors.New("list dir requires 1 or 0 arguments")
	}
	fullDirPath = expandRemotePath(ctx, fullDirPath)
	if fullDirPath == "/" {
		fullDirPath = ""
	}
	st, msg, info := getApi(ctx).ListDir(fullDirPath)
	//fmt.Printf("Children of \"%s\"\n",fullDirPath)
	if st == api.Status_ERROR {
		log.Println(msg)
	} else {
		for iDir := range info.Subdirs {
			fmt.Println("DIR ", info.Subdirs[iDir][len(fullDirPath)+1:])
		}
		for iFile := range info.Files {
			fmt.Println("FILE", info.Files[iFile][len(fullDirPath)+1:])
		}
	}
	return nil
}

// mkdir <remote dir name>
func DoCreateDir(ctx context.Context, args []string) error {
	if len(args) != 1 {
		return errors.New("create dir requires exactly 1 argument")
	}
	newFullDirPath := expandRemotePath(ctx, args[0])
	st, msg := getApi(ctx).CreateDir(newFullDirPath)
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

// rmdir <remote dir name>
func DoRemoveDir(ctx context.Context, args []string) error {
	if len(args) != 1 {
		return errors.New("remove dir requires exactly 1 argument")
	}
	fullDirPath := expandRemotePath(ctx, args[0])
	st, msg := getApi(ctx).RemoveDir(fullDirPath, false)
	if st == api.Status_OK {
		return nil
	}
	if st == api.Status_ERROR && msg == "directory not empty, use force" {
		fmt.Println("Directory is not empty, do you really want to delete it? [y/N]")
		clientReader := bufio.NewReader(os.Stdin)
		lineBytes, _, err := clientReader.ReadLine()
		if err != nil {
			println("Confirmation read error occurred : ", err)
			return err
		}
		lineString := string(lineBytes)
		if lineString == "y" || lineString == "Y" {
			st, msg := getApi(ctx).RemoveDir(fullDirPath, true)
			if st == api.Status_ERROR {
				log.Println(msg)
				return nil
			}
			return nil
		} else {
			fmt.Println("Operation aborted")
			clientReader = nil
		}
	}
	if st == api.Status_ERROR {
		log.Println(msg)
	}
	return nil
}

func DoShowHelp(ctx context.Context, args []string) error {
	fmt.Println("GoDFS client commands help")
	fmt.Println("  init \n\tinitializes file system from scratch")
	fmt.Println("  touch <remote path> \n\tcreates file in the remote file system")
	fmt.Println("  read <remote path> <local path> \n\tsaves file from remote file system into local one")
	fmt.Println("  write <local path> <remote path> \n\tuploads file from local file system into remote")
	fmt.Println("  rm <remote path>\n\tremoves file from remote file system")
	fmt.Println("  info <remote path>\n\tprint information about file")
	fmt.Println("  cp <remote path> <copy remote path>\n\tcreates copy of file with the new name")
	fmt.Println("  mv <remote path> <new remote path>\n\tmoves file to the new location")
	fmt.Println("  cd <remote path>\n\tchanges current remote working directory")
	fmt.Println("  ls <remote path>\n\tlist contents of remote directory")
	fmt.Println("  mkdir <remote path>\n\tcreates new remote directory")
	fmt.Println("  rmdir <remote path>\n\tremoves remote directory with confirmation, if it is not empty")
	fmt.Println("  exit\n\tstop client process")
	fmt.Println("  help\n\tprints this message")
	fmt.Println("Accepted paths: relative (name1/name2), absolute (/name1/name2), current dir (.), parent dir (..)")
	return nil
}

// exit
func ClientExit(ctx context.Context, args []string) error {
	os.Exit(0)
	return nil
}
