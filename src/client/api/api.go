package api

import nnapi "gitlab.com/godfs-team/godfs/src/namenode/api"

type Status int32

const (
	Status_OK    Status = 0
	Status_ERROR Status = 1
)

type ClientApi interface {
	InitStorage() (Status, string)
	CreateFile(remoteFilePath string) (Status, string)
	ReadFile(remoteFilePath string, localFilePath string) (Status, string)
	WriteFile(localFilePath string, remoteFilePath string) (Status, string)
	RemoveFile(remoteFilePath string) (Status, string)
	GetFileInfo(remoteFileName string) (Status, string, *nnapi.FileInfo)
	CopyFile(remoteFilePathSource string, remoteFileCopyPathTarget string) (Status, string)
	MoveFile(remoteFilePathSource string, remoteFileCopyPathTarget string) (Status, string)
	ChangeDirLocal(newFullDirPath string) (Status, string)
	ChangeDirRemote(newFullDirPath string) (Status, string)
	ListDir(fullDirPath string) (Status, string, *nnapi.DirInfo)
	CreateDir(newFullDirPath string) (Status, string)
	RemoveDir(fullDirPath string, withForce bool) (Status, string)
}
