package main

import (
	"bufio"
	"context"
	"fmt"
	"gitlab.com/godfs-team/godfs/src/client/service"
	"gitlab.com/godfs-team/godfs/src/client/shell"
	"log"
	"os"
	"strings"
)

var functionMap = make(map[string]func(context.Context, []string) error)
var clientState *shell.ClientState

func init() {
	functionMap["init"] = shell.DoInitStorage
	functionMap["touch"] = shell.DoCreateFile
	functionMap["read"] = shell.DoReadFile
	functionMap["write"] = shell.DoWriteFile
	functionMap["rm"] = shell.DoRemoveFile
	functionMap["info"] = shell.GetFileInfo
	functionMap["cp"] = shell.DoCopyFile
	functionMap["mv"] = shell.DoMoveFile
	functionMap["cd"] = shell.DoChangeDir
	functionMap["ls"] = shell.DoListDir
	functionMap["mkdir"] = shell.DoCreateDir
	functionMap["rmdir"] = shell.DoRemoveDir
	functionMap["exit"] = shell.ClientExit
	functionMap["help"] = shell.DoShowHelp
}

func main() {
	workDir, err := os.Getwd()
	if err != nil {
		log.Println("Unable to get client workdir, strange")
		workDir = ""
	}
	clientState = &shell.ClientState{
		CurrentLocalFolder:  workDir,
		CurrentRemoteFolder: "",
	}
	clientReader := bufio.NewReader(os.Stdin)
	parentContext := context.TODO()
	parentContext = context.WithValue(parentContext, "api", service.ClientApiServer{})
	shellContext := context.WithValue(parentContext, "state", clientState)
	fmt.Println("Go dFS client started")
	for {
		func() {
			_ = os.Stdout.Sync()
			_ = os.Stderr.Sync()
			localDir := shellContext.Value("state").(*shell.ClientState).CurrentLocalFolder + "/"
			remoteDir := shellContext.Value("state").(*shell.ClientState).CurrentRemoteFolder + "/"
			fmt.Println("\033[32;0m" + localDir + "\033[35;0m:\033[32;0m" + remoteDir + "\033[0m")
		}()
		lineBytes, _, err := clientReader.ReadLine()
		if err != nil {
			println("Error occurred : ", err)
			break
		}
		lineString := string(lineBytes)
		lineArray := strings.Split(lineString, " ")
		command := lineArray[0]
		var commandArgs []string
		if len(lineArray) > 1 {
			commandArgs = lineArray[1:]
		} else {
			commandArgs = make([]string, 0)
		}
		commandFunction, commandFound := functionMap[command]
		if !commandFound {
			println("Command ", command, " was not found")
			continue
		}
		err = commandFunction(shellContext, commandArgs)
		if err != nil {
			println("Command returned error : ", err.Error())
		}
	}
}
