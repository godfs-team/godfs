package service

import (
	"context"
	"gitlab.com/godfs-team/godfs/src/client/api"
	"google.golang.org/grpc"
	"log"
)
import nnapi "gitlab.com/godfs-team/godfs/src/namenode/api"

func (ClientApiServer) ListDir(fullDirPath string) (api.Status, string, *nnapi.DirInfo) {
	if fullDirPath == "/" {
		// root directory have no name
		fullDirPath = ""
	}
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	dirEntity := nnapi.NSEntity{
		Name:     fullDirPath,
		Type:     nnapi.NamespaceEntityType_DIR,
		DirInfo:  nil,
		FileInfo: nil,
	}
	response, err := nnClient.NamespaceEntityInfo(context.TODO(), &dirEntity)
	if err != nil {
		return api.Status_ERROR, err.Error(), nil
	}
	if response.Type != nnapi.NamespaceEntityType_DIR {
		return api.Status_ERROR, response.Msg, nil
	}
	if response.Status == nnapi.Status_ERROR {
		return api.Status_ERROR, response.Msg, nil
	}
	return api.Status_OK, response.Msg, response.DirInfo
}

func (ClientApiServer) CreateDir(newFullDirPath string) (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	createEntity := nnapi.NSEntity{
		Name:     newFullDirPath,
		Type:     nnapi.NamespaceEntityType_DIR,
		DirInfo:  nil,
		FileInfo: nil,
	}
	createRequest := nnapi.NSChangeRequest{
		Operation: nnapi.ChangeOperationType_ADD,
		Subjects:  []*nnapi.NSEntity{&createEntity},
	}
	response, err := nnClient.ChangeNamespaceEntity(context.TODO(), &createRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == nnapi.Status_OK {
		return api.Status_OK, response.Msg
	}
	return api.Status_ERROR, response.Msg
}

func (ClientApiServer) RemoveDir(fullDirPath string, withForce bool) (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	removeEntity := nnapi.NSEntity{
		Name:     fullDirPath,
		Type:     nnapi.NamespaceEntityType_DIR,
		DirInfo:  nil,
		FileInfo: nil,
	}
	removeRequest := nnapi.NSChangeRequest{
		Operation: nnapi.ChangeOperationType_REMOVE,
		WithForce: withForce,
		Subjects:  []*nnapi.NSEntity{&removeEntity},
	}
	response, err := nnClient.ChangeNamespaceEntity(context.TODO(), &removeRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == nnapi.Status_OK {
		return api.Status_OK, response.Msg
	}
	withForceStr := "REMOVE operation failed: directory not empty, use force"
	if response.Status == nnapi.Status_ERROR && response.Msg == withForceStr {
		return api.Status_ERROR, "directory not empty, use force"
	}
	return api.Status_ERROR, response.Msg
}
