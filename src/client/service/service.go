package service

import (
	"context"
	"gitlab.com/godfs-team/godfs/src/client/api"
	NamenodeApi "gitlab.com/godfs-team/godfs/src/namenode/api"
	"google.golang.org/grpc"
	"log"
)

type ClientApiServer struct {
	State struct {
		LocalDir  string
		RemoteDir string
	}
}

func (ClientApiServer) InitStorage() (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	nnClient := NamenodeApi.NewNamenodeClient(nnConn)
	initRequest := NamenodeApi.InitRequest{}
	response, err := nnClient.Init(context.TODO(), &initRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == NamenodeApi.Status_OK {
		return api.Status_OK, response.Msg
	}
	return api.Status_ERROR, response.Msg
}
