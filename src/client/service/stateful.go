package service

import (
	"gitlab.com/godfs-team/godfs/src/client/api"
)

func (srv ClientApiServer) ChangeDirLocal(newFullDirPath string) (api.Status, string) {
	srv.State.LocalDir = newFullDirPath
	return api.Status_OK, ""
}
func (srv ClientApiServer) ChangeDirRemote(newFullDirPath string) (api.Status, string) {
	// try to find such directory in the namespace
	status, msg, _ := srv.ListDir(newFullDirPath)
	if status != api.Status_OK {
		return status, "unable to get info about directory: " + msg
	}
	srv.State.RemoteDir = newFullDirPath
	return api.Status_OK, ""
}
