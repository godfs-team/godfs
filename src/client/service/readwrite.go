package service

import (
	"context"
	"fmt"
	"gitlab.com/godfs-team/godfs/src/client/api"
	dnapi "gitlab.com/godfs-team/godfs/src/datanode/api"
	nnapi "gitlab.com/godfs-team/godfs/src/namenode/api"
	"google.golang.org/grpc"
	"io"
	"log"
	"os"
	"strings"
)

func unpackAddress(datanodeAddr string) string {
	if strings.HasPrefix(datanodeAddr, "0.0.0.0") {
		return "127.0.0.1:" + strings.Split(datanodeAddr, ":")[1]
	}
	return datanodeAddr
}

func (ClientApiServer) ReadFile(remoteFilePath string, localFilePath string) (api.Status, string) {
	//Get info from namenode first
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	readEntity := nnapi.NSEntity{
		Name:     remoteFilePath,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	entityInfoResponse, err := nnClient.NamespaceEntityInfo(context.TODO(), &readEntity)
	_ = nnConn.Close()
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	fmt.Printf("ReadFile:NamespaceEntityInfo Request: %v: %v\n", entityInfoResponse.Status, entityInfoResponse.Msg)
	if entityInfoResponse.Status == nnapi.Status_ERROR {
		return api.Status_ERROR, entityInfoResponse.Msg
	}
	if entityInfoResponse.Type != nnapi.NamespaceEntityType_FILE {
		return api.Status_ERROR, entityInfoResponse.Msg
	}
	datanode := entityInfoResponse.FileInfo.DataNodes[0]
	dnConn, err := grpc.Dial(unpackAddress(datanode.ServerAddress), grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	dnClient := dnapi.NewDatanodeClient(dnConn)
	readRequest := dnapi.ReadRequest{
		UUID:    entityInfoResponse.FileInfo.GetFileUUID(),
		Offset:  0,
		MaxSize: -1,
	}
	readClient, err := dnClient.Read(context.TODO(), &readRequest)
	if err != nil {
		log.Printf("Unable to open read connection to the datanode: %v\n", err.Error())
		return api.Status_ERROR, "read connection failed"
	}
	file, err := os.OpenFile(localFilePath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		log.Println("Unable to open local file", localFilePath, "in read mode:", err)
		return api.Status_ERROR, "unable to open local file"
	}
	defer func() {
		_ = file.Close()
	}()
	for {
		readMsg, err := readClient.Recv()
		if err != nil {
			if err == io.EOF {
				//reading done
				break
			} else {
				log.Printf("Unable to read data block: %v", err.Error())
				return api.Status_ERROR, "read data block failed"
			}
		}
		if readMsg.Response == nil {
			return api.Status_ERROR, "read block response is empty"
		}
		if readMsg.Response.Status == dnapi.DatanodeStatus_ERROR {
			return api.Status_ERROR, "read block returned error" + readMsg.Response.Msg
		}
		// all correct, save file to the client local storage
		_, err = file.Write(readMsg.Chunk)
		if err != nil {
			log.Printf("Unable to save incoming file chunk: %v", err.Error())
			return api.Status_ERROR, err.Error()
		} else {
			fmt.Println("Received and saved chunk of size", len(readMsg.Chunk))
		}
	}
	return api.Status_OK, "ok"
}

// TODO: DONE??
func (ClientApiServer) WriteFile(localFilePath string, remoteFilePath string) (api.Status, string) {
	//Get info from namenode first
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	if err != nil {
		log.Printf("Could not connect: %v", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	writeEntity := nnapi.NSEntity{
		Name:     remoteFilePath,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	entityInfoResponse, err := nnClient.NamespaceEntityInfo(context.TODO(), &writeEntity)
	_ = nnConn.Close()
	if err != nil {
		log.Printf("Could not perform WriteFile:NamespaceEntityInfo Request: %v\n", err)
		return api.Status_ERROR, err.Error()
	}
	fmt.Printf("WriteFile:NamespaceEntityInfo Request: %v: %v\n", entityInfoResponse.Status, entityInfoResponse.Msg)
	if entityInfoResponse.Status == nnapi.Status_ERROR {
		return api.Status_ERROR, entityInfoResponse.Msg
	}
	if entityInfoResponse.Type != nnapi.NamespaceEntityType_FILE {
		return api.Status_ERROR, entityInfoResponse.Msg
	}
	datanode := entityInfoResponse.FileInfo.DataNodes[0]
	dnConn, err := grpc.Dial(unpackAddress(datanode.ServerAddress), grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v", err)
	}
	dnClient := dnapi.NewDatanodeClient(dnConn)
	writeClient, err := dnClient.Write(context.TODO())
	if err != nil {
		log.Printf("Unable to open write connection to the datanode: %v\n", err.Error())
		return api.Status_ERROR, "write connection failed"
	}
	file, err := os.OpenFile(localFilePath, os.O_RDONLY, 0666)
	if err != nil {
		log.Println("Unable to open local file", localFilePath, "to perform write operation to the datanode:", err)
		return api.Status_ERROR, "local file can not be open"
	}
	defer func() {
		_ = file.Close()
	}()
	for {
		// read file from the client local storage
		rawChunk := make([]byte, 1024)
		n, err := file.Read(rawChunk)
		if err == io.EOF {
			//reading done
			break
		}
		if err != nil {
			log.Printf("Unable to read outgoing file chunk: %v\n", err.Error())
			return api.Status_ERROR, err.Error()
		}
		chunk := rawChunk[:n]
		writeRequest := dnapi.WriteRequest{
			UUID:   entityInfoResponse.FileInfo.FileUUID,
			Offset: 0,
			Chunk:  chunk,
		}
		fmt.Println("Sending data block of size", len(chunk), "into remote file", entityInfoResponse.FileInfo.FileUUID)
		err = writeClient.Send(&writeRequest)
		if err != nil {
			log.Printf("Unable to write data block: %v\n", err.Error())
			return api.Status_ERROR, "write data block failed"
		}
	}
	response, err := writeClient.CloseAndRecv()
	if err != nil {
		return api.Status_ERROR, "datanode write func returned error:" + err.Error()
	}
	if response.Status != dnapi.DatanodeStatus_OK {
		return api.Status_ERROR, "datanode returned not-ok status:" + response.Msg
	}
	return api.Status_OK, "ok"
}
