package service


import "os"

var NamenodeAddress = func() string {
	addr, passed := os.LookupEnv("NAMENODE_ADDRESS")
	if !passed {
		return "namenode:8000"
	} else {
		return addr
	}
}()