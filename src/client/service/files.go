package service

import (
	"context"
	"gitlab.com/godfs-team/godfs/src/client/api"
	nnapi "gitlab.com/godfs-team/godfs/src/namenode/api"
	"google.golang.org/grpc"
	"log"
)

func (ClientApiServer) CreateFile(remoteFilePath string) (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	createEntity := nnapi.NSEntity{
		Name:     remoteFilePath,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	createRequest := nnapi.NSChangeRequest{
		Operation: nnapi.ChangeOperationType_ADD,
		Subjects:  []*nnapi.NSEntity{&createEntity},
	}
	response, err := nnClient.ChangeNamespaceEntity(context.TODO(), &createRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == nnapi.Status_OK {
		return api.Status_OK, response.Msg
	}
	return api.Status_ERROR, response.Msg
}

func (ClientApiServer) RemoveFile(remoteFilePath string) (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	removeEntity := nnapi.NSEntity{
		Name:     remoteFilePath,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	removeRequest := nnapi.NSChangeRequest{
		Operation: nnapi.ChangeOperationType_REMOVE,
		Subjects:  []*nnapi.NSEntity{&removeEntity},
	}
	response, err := nnClient.ChangeNamespaceEntity(context.TODO(), &removeRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == nnapi.Status_OK {
		return api.Status_OK, response.Msg
	}
	return api.Status_ERROR, response.Msg
}

func (ClientApiServer) GetFileInfo(remoteFileName string) (api.Status, string, *nnapi.FileInfo) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect to namenode: %v\n", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	fileEntity := nnapi.NSEntity{
		Name:     remoteFileName,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	response, err := nnClient.NamespaceEntityInfo(context.TODO(), &fileEntity)
	if err != nil {
		return api.Status_ERROR, err.Error(), nil
	}
	if response.Status != nnapi.Status_OK {
		return api.Status_ERROR, response.Msg, nil
	}
	return 0, "", response.FileInfo
}

func (ClientApiServer) CopyFile(remoteFilePathSource string, remoteFileCopyPathTarget string) (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	sourceEntity := nnapi.NSEntity{
		Name:     remoteFilePathSource,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	targetEntity := nnapi.NSEntity{
		Name:     remoteFileCopyPathTarget,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	copyRequest := nnapi.NSChangeRequest{
		Operation: nnapi.ChangeOperationType_COPY,
		Subjects:  []*nnapi.NSEntity{&sourceEntity, &targetEntity},
	}
	response, err := nnClient.ChangeNamespaceEntity(context.TODO(), &copyRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == nnapi.Status_OK {
		return api.Status_OK, response.Msg
	}
	return api.Status_ERROR, response.Msg
}

func (ClientApiServer) MoveFile(remoteFilePathSource string, remoteFileCopyPathTarget string) (api.Status, string) {
	nnConn, err := grpc.Dial(NamenodeAddress, grpc.WithInsecure())
	defer func() {
		_ = nnConn.Close()
	}()
	if err != nil {
		log.Printf("Could not connect: %v\n", err)
	}
	nnClient := nnapi.NewNamenodeClient(nnConn)
	sourceEntity := nnapi.NSEntity{
		Name:     remoteFilePathSource,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	targetEntity := nnapi.NSEntity{
		Name:     remoteFileCopyPathTarget,
		Type:     nnapi.NamespaceEntityType_FILE,
		DirInfo:  nil,
		FileInfo: nil,
	}
	moveRequest := nnapi.NSChangeRequest{
		Operation: nnapi.ChangeOperationType_MOVE,
		Subjects:  []*nnapi.NSEntity{&sourceEntity, &targetEntity},
	}
	response, err := nnClient.ChangeNamespaceEntity(context.TODO(), &moveRequest)
	if err != nil {
		return api.Status_ERROR, err.Error()
	}
	if response.Status == nnapi.Status_OK {
		return api.Status_OK, response.Msg
	}
	return api.Status_ERROR, response.Msg
}
