package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	dnapi "gitlab.com/godfs-team/godfs/src/datanode/api"
	"google.golang.org/grpc"
	"log"
)

func BeginFullReplication(sourceFileInfo FileInfo, targetFileInfo FileInfo) (err error) {
	//TODO: target file is created on all tardet datanodes
	//TODO: run content copy from source datanodes to targets
	sourceFileUuidStr := sourceFileInfo.FileUUID.String()
	targetFileUuidStr := targetFileInfo.FileUUID.String()
	if len(sourceFileInfo.StorageUUIDs) != len(targetFileInfo.StorageUUIDs) {
		return errors.New("copy logic error: replica count for target and source file should be equal")
	}
	for i := range sourceFileInfo.StorageUUIDs {
		targetDatanodeUuid := targetFileInfo.StorageUUIDs[i]
		sourceDatanodeInfo, err := GetDatanodeInfoByUuid(sourceFileInfo.StorageUUIDs[i])
		if err != nil {
			return errors.New("unable to get info for FILE copy source datanode:" + err.Error())
		}
		targetDatanodeInfo, err := GetDatanodeInfoByUuid(targetDatanodeUuid)
		if err != nil {
			return errors.New("unable to get info for FILE copy target datanode:" + err.Error())
		}
		targetConn, err := grpc.Dial(sourceDatanodeInfo.NodePrivateAddress, grpc.WithInsecure())
		dnClient := dnapi.NewDatanodePrivateClient(targetConn)
		replicationResponse, err := dnClient.Replicate(context.TODO(), &dnapi.ReplicationTask{
			TaskUuid: uuid.New().String(),
			Source: &dnapi.ReplicationBlock{
				DatanodeAddress: sourceDatanodeInfo.NodePrivateAddress,
				DataUuid:        sourceFileUuidStr,
				DataStartByte:   0,
				DataEndByte:     -1,
			},
			Target: &dnapi.ReplicationBlock{
				DatanodeAddress: targetDatanodeInfo.NodePrivateAddress,
				DataUuid:        targetFileUuidStr,
				DataStartByte:   0,
				DataEndByte:     -1,
			},
		})
		if err != nil {
			return errors.New("copy replication failed with error: " + err.Error())
		}
		if replicationResponse.Status != dnapi.DatanodeStatus_OK {
			return errors.New("copy replication failed with non-ok status: " + replicationResponse.Msg)
		}
	}
	return nil
}

func BeginDatanodeReplication(datanodeUuid uuid.UUID) (err error) {
	//TODO: retrieve all files of fallen datanode
	//TODO: for each file locate its copy, create it on chosen datanode and send replication request to the replica holder
	datanodeUuidStr := datanodeUuid.String()
	filesInDatanodeKey := "list:files:" + datanodeUuidStr + ":id"
	filesInDatanode, err := redisClient.LRange(filesInDatanodeKey, 0, -1).Result()
	allDatanodes, err := GetAllDatanodes()
	if err != nil {
		log.Println("Unable to retrieve all datanodes for DB:", err.Error())
		return errors.New("unable to retrieve all datanodes list: " + err.Error())
	}
	for i := 0; i < len(filesInDatanode); i++ {
		fileUuidStr := filesInDatanode[i]
		fileUuid, _ := uuid.Parse(fileUuidStr)
		fileInfo, err := GetFileInfoByUuid(fileUuid)
		if err != nil {
			log.Println("Unable to get file info while datanode replication: " + err.Error())
			return errors.New("unable to get file info of fallen datanode: " + err.Error())
		} else {
			log.Print("Setting up strategy to make replica of file", fileInfo.FileUUID, "from datanode", datanodeUuidStr)
			log.Print("; stored on ")
			for i := range fileInfo.StorageUUIDs {
				log.Print(fileInfo.StorageUUIDs[i].String() + ", ")
			}
			log.Println()
		}
		var fileReplicationSource *DatanodeInfo
		var fileReplicationTarget *DatanodeInfo
		for j := 0; j < len(allDatanodes); j++ {
			candidateDatanode := allDatanodes[j]
			log.Println("computing candidate datanode ", candidateDatanode.NodeUuid)
			if candidateDatanode.NodeUuid == datanodeUuid {
				log.Println("  datanode is to be skipped, it is the fallen one")
				continue
			}
			foundInSources := false
			for k := 0; k < len(fileInfo.StorageUUIDs); k++ {
				log.Println("  checking candidate datanode", candidateDatanode.NodeUuid, "and storing datanode", fileInfo.StorageUUIDs[k])
				if candidateDatanode.NodeUuid == fileInfo.StorageUUIDs[k] {
					log.Println("  candidate datanode will be used as source for file ", fileUuidStr)
					// this datanode can be used as replication source, as it stores file to replicate
					fileReplicationSource = candidateDatanode
					foundInSources = true
				}
			}
			if !foundInSources {
				log.Println("  candidate datanode is not a file source yet, so it will be replica target")
				// this datanode is not storing this file, it can be used as replication target
				fileReplicationTarget = candidateDatanode
			}
		}
		if fileReplicationSource == nil {
			log.Println("Unable to find replication source datanode")
			return errors.New("unable to find source datanode for file " + fileUuidStr + " to be replicated from " + datanodeUuidStr)
		}
		if fileReplicationTarget == nil {
			log.Println("Unable to find replication target datanode")
			return errors.New("unable to find target datanode for file" + fileUuidStr + " to be replicated from " + datanodeUuidStr)
		}

		// target found, create file on it
		targetConn, err := grpc.Dial(fileReplicationTarget.NodePrivateAddress, grpc.WithInsecure())
		dnClient := dnapi.NewDatanodePrivateClient(targetConn)
		createRequest := dnapi.CreateRequest{
			UUID: fileUuidStr,
		}
		response, err := dnClient.Create(context.TODO(), &createRequest)
		if err != nil {
			_ = targetConn.Close()
			log.Println("Unable to create file ", fileUuidStr, "to replicate on the target datanode", fileReplicationTarget.NodeUuid.String())
			return errors.New("Unable to create file on the target replication datanode: " + err.Error())
		}
		if response.Status != dnapi.DatanodeStatus_OK {
			_ = targetConn.Close()
			return errors.New("Unable to create file on the target replication datanode : " + response.Msg)
		}
		err = repositoryAddFileToDatanode(fileReplicationTarget.NodeUuid.String(), fileUuidStr)
		if err != nil {
			return errors.New("Unable to add new target datanode as file storage to database: " + err.Error())
		}
		// source and target are found, files prepared, send replication request to the source datanode
		sourcePrivateAddress := fileReplicationSource.NodePrivateAddress
		sourceConn, err := grpc.Dial(sourcePrivateAddress, grpc.WithInsecure())
		if err != nil {
			_ = targetConn.Close()
			log.Println("Replication source at", sourcePrivateAddress, "unable to be connected, to much replication logic, let's panic")
			return errors.New("unable to connect to the source replication datanode: " + err.Error())
		}
		sourceClient := dnapi.NewDatanodePrivateClient(sourceConn)
		replicationResponse, err := sourceClient.Replicate(context.TODO(), &dnapi.ReplicationTask{
			TaskUuid: uuid.New().String(),
			Source: &dnapi.ReplicationBlock{
				DatanodeAddress: fileReplicationSource.NodePrivateAddress,
				DataUuid:        fileUuidStr,
				DataStartByte:   0,
				DataEndByte:     -1,
			},
			Target: &dnapi.ReplicationBlock{
				DatanodeAddress: fileReplicationTarget.NodePrivateAddress,
				DataUuid:        fileUuidStr,
				DataStartByte:   0,
				DataEndByte:     -1,
			},
		})
		_ = targetConn.Close()
		_ = sourceConn.Close()
		if err != nil {
			return errors.New("Unable to replicate: source returned error" + err.Error())
		}
		if replicationResponse.Status != dnapi.DatanodeStatus_OK {
			return errors.New("Unable to replicate: source returned not-ok: " + replicationResponse.Msg)
		}
		fmt.Println("replicated file", fileUuidStr, "from", fileReplicationSource.NodeUuid, "to", fileReplicationTarget.NodeUuid)
	}
	//it will be super cool, if our code will run till here
	fmt.Println("Holy cow, backup replication for datanode", datanodeUuidStr, "is done! Unbelievable")
	return nil
}
