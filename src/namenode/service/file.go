package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
	dnapi "gitlab.com/godfs-team/godfs/src/datanode/api"
	"google.golang.org/grpc"
	"log"
	"math/rand"
	"strconv"
	"time"
)

/*
For each FileInfo

name:<fileName>:id -> string uuid of file
id:<fileUuid>:type -> type of entity (FILE)
id:<fileUuid>:info -> hash map of string values{ FullName, FileUUUID, Size]
id:<fileUuid>:info:StorageUUIDs -> list of datanode uuids
*/

type FileInfo struct {
	// full name of the file, including the path
	FullName string
	// file uuid (for datanodes access)
	FileUUID uuid.UUID
	// file size
	Size int64
	// uuids of datanodes which stores this file
	StorageUUIDs []uuid.UUID
}

func GetFileInfo(fileName string) (fileInfo FileInfo, err error) {
	// return file info from namenode database
	fileUuidStr, err := redisClient.Get("name:" + fileName + ":id").Result()
	if err == redis.Nil {
		return fileInfo, errors.New("name " + fileName + " is not found in the namespace")
	}

	fileUuid, err := uuid.Parse(fileUuidStr)
	if err != nil {
		panic("name:" + fileName + ":id stores incorrect value " + fileUuidStr + " instead of uuid")
	}
	return GetFileInfoByUuid(fileUuid)
}

func GetFileInfoByUuid(fileUuid uuid.UUID) (fileInfo FileInfo, err error) {
	fileUuidStr := fileUuid.String()
	typeStr, err := redisClient.Get("id:" + fileUuidStr + ":type").Result()
	if err == redis.Nil {
		return fileInfo, errors.New("type for uuid " + fileUuidStr + " is not found in the namespace")
	} else if typeStr != "FILE" {
		return fileInfo, errors.New("GetFileInfo can not be applied to entity of type " + typeStr)
	}

	infoBaseStr := "id:" + fileUuidStr + ":info"
	infoMap, err := redisClient.HGetAll(infoBaseStr).Result()
	if err == redis.Nil {
		return fileInfo, errors.New("unable to find info for FILE " + fileUuidStr)
	}

	fileInfo.FullName = infoMap["FullName"]
	fileInfoUuid, _ := uuid.Parse(infoMap["FileUUID"])
	fileInfo.FileUUID = fileInfoUuid
	fileInfoSize, _ := strconv.ParseInt(infoMap["Size"], 10, 64)
	fileInfo.Size = fileInfoSize

	storageStrList, err := redisClient.LRange(infoBaseStr+":StorageUUIDs", 0, -1).Result()
	if err == redis.Nil {
		return fileInfo, errors.New("unable to find StorageUUIDs for FILE " + fileUuidStr)
	}
	storageUuidList := make([]uuid.UUID, len(storageStrList))
	for i := 0; i < len(storageStrList); i++ {
		storageUuidList[i], _ = uuid.Parse(storageStrList[i])
	}
	fileInfo.StorageUUIDs = storageUuidList

	return fileInfo, nil
}

func selectAndSendCreationRequests(info FileInfo) (selectedDatanodesUuid []uuid.UUID, err error) {
	datanodes, err := GetAllDatanodes()
	if err != nil {
		return selectedDatanodesUuid, err
	}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(datanodes), func(i, j int) { datanodes[i], datanodes[j] = datanodes[j], datanodes[i] })
	i := 0
	for ; i < MinimumReplicaCount && i < len(datanodes); i++ {
		nnConn, err := grpc.Dial(datanodes[i].NodePrivateAddress, grpc.WithInsecure())
		defer func() {
			_ = nnConn.Close()
		}()

		if err != nil {
			log.Printf("Could not connect: %v", err)
			//TODO: ping datanode
			i--
		}
		dnClient := dnapi.NewDatanodePrivateClient(nnConn)
		createRequest := dnapi.CreateRequest{
			UUID: info.FileUUID.String(),
		}
		response, err := dnClient.Create(context.TODO(), &createRequest)
		if err != nil {
			log.Printf("Could not perform Create Request: %v", err)
			i--
		} else if response.Status == dnapi.DatanodeStatus_OK {
			selectedDatanodesUuid = append(selectedDatanodesUuid, datanodes[i].NodeUuid)
		} else {
			i--
		}
	}
	if i != MinimumReplicaCount {
		fmt.Println("!!need additional datanode to satisfy minimum replica count")
		return selectedDatanodesUuid, errors.New("not enough datanodes")
		//TODO: panic
	}
	return selectedDatanodesUuid, nil

}

func CreateFile(fileName string) (fileInfo FileInfo, err error) {
	// put file info into namenode database
	// create new file with generated UUID on datanodes
	fileUuid := uuid.New()
	fileUuidStr := fileUuid.String()
	fileInfo.FullName = fileName
	fileInfo.FileUUID = fileUuid
	fileInfo.Size = int64(0)

	// add dir to the parent dir
	subDirInfo, err := getEntityParentDir(fileName)
	if err != nil {
		fmt.Println("Unable to find parent DIR for the creating FILE", fileName, ": ", err)
		return fileInfo, err
	}
	_, err = redisClient.LPush("id:"+subDirInfo.DirUUID.String()+":info:FileList", fileName).Result()
	if err != nil {
		fmt.Println("Unable to add creating FILE to the parent DIR", subDirInfo.FullName, ": ", err)
		return fileInfo, err
	}
	fileInfo.StorageUUIDs, err = selectAndSendCreationRequests(fileInfo)

	_, _ = redisClient.Set("name:"+fileName+":id", fileUuidStr, 0).Result()
	_, _ = redisClient.Set("id:"+fileUuidStr+":type", "FILE", 0).Result()
	infoMap := map[string]interface{}{
		"FullName": fileInfo.FullName,
		"FileUUID": fileInfo.FileUUID.String(),
		"Size":     strconv.FormatInt(fileInfo.Size, 10),
	}
	redisClient.HMSet("id:"+fileUuidStr+":info", infoMap)

	// storageUUIDsStr := make([]string, len(fileInfo.StorageUUIDs))
	for i := 0; i < len(fileInfo.StorageUUIDs); i++ {
		uuidStr := fileInfo.StorageUUIDs[i].String()
		err := repositoryAddFileToDatanode(uuidStr, fileUuidStr)
		if err != nil {
			return fileInfo, err
		}
	}
	// redisClient.LPush("id:"+fileUuidStr+":info:StorageUUIDs", storageUUIDsStr)
	return fileInfo, err
}

func RemoveFile(fileName string) error {
	// get file UUID
	// remove file data from all datanodes
	fileInfo, err := GetFileInfo(fileName)
	if err != nil {
		return errors.New("nested error: " + err.Error())
	}
	fileUuidStr := fileInfo.FileUUID.String()
	infoBaseStr := "id:" + fileUuidStr + ":info"

	keysToRemove := []string{
		"name:" + fileName + ":id",
		"id:" + fileUuidStr + ":type",
		infoBaseStr,
		infoBaseStr + ":StorageUUIDs",
	}
	deletedCount, err := redisClient.Del(keysToRemove...).Result()
	expectedCount := int64(len(keysToRemove))
	if deletedCount != expectedCount {
		deletedCountStr := strconv.Itoa(int(deletedCount))
		expectedCountStr := strconv.Itoa(int(expectedCount))
		return errors.New("deleted values mismatch while FILE removal: expected " + expectedCountStr + ", removed " + deletedCountStr)
	}
	// remove the file from parent directory
	parentDir, err := getEntityParentDir(fileName)
	if err != nil {
		return errors.New("unable to lookup parent entity of removing file: " + err.Error())
	}
	redisClient.LRem("id:"+parentDir.DirUUID.String()+":info:FileList", 1, fileName)
	//TODO: call storing datanodes to remove file contents
	for i := range fileInfo.StorageUUIDs {
		datanodeInfo, err := GetDatanodeInfoByUuid(fileInfo.StorageUUIDs[i])
		if err != nil {
			return errors.New("unable to get info about file storing datanode to remove:" + err.Error())
		}
		dnConn, err := grpc.Dial(datanodeInfo.NodePrivateAddress, grpc.WithInsecure())
		dnClient := dnapi.NewDatanodePrivateClient(dnConn)
		deleteResponse, err := dnClient.Delete(context.TODO(), &dnapi.DeleteRequest{
			UUID: fileUuidStr,
		})
		datanodeUuidStr := datanodeInfo.NodeUuid.String()
		if err != nil {
			return errors.New("error while removing file " + fileUuidStr + "from datanode " + datanodeUuidStr + ": " + err.Error())
		}
		if deleteResponse.Status != dnapi.DatanodeStatus_OK {
			return errors.New("datanode " + datanodeUuidStr + " returned not-ok status on file " + fileUuidStr + " removal: " + deleteResponse.Msg)
		}
		// all fine, remove datanode<->file pair from database
		err = repositoryRemoveFileFromDatanode(datanodeUuidStr, fileUuidStr)
		if err != nil {
			return errors.New("remove failed while removing file:datanode pair from database: " + err.Error())
		}
	}

	return nil
}

func CopyFile(fileName string, copyName string) error {
	// generate new file entry, take UUID
	// each datanode should make copy of file data with new UUID
	fileInfo, err := GetFileInfo(fileName)
	if err != nil {
		return errors.New("nested error: " + err.Error())
	}
	copyInfo, err := CreateFile(copyName)
	if err != nil {
		return errors.New("nested error: " + err.Error())
	}
	err = BeginFullReplication(fileInfo, copyInfo)
	if err != nil {
		return errors.New("nested error: " + err.Error())
	}
	return nil
}

func MoveFile(fileName string, newFileName string) error {
	// ensure we have a parent for new file path
	newParentDir, err := getEntityParentDir(newFileName)
	if err != nil {
		return errors.New("unable to retrieve parent dir for new file path: " + err.Error())
	}
	// change file name in local database
	fileUuidStr, err := redisClient.Get("name:" + fileName + ":id").Result()
	if err == redis.Nil {
		return errors.New("name " + fileName + " is not found in the namespace")
	}

	infoBaseStr := "id:" + fileUuidStr + ":info"
	redisClient.HSet(infoBaseStr, "FileName", newFileName)
	redisClient.Del("name:" + fileName + ":id")
	redisClient.Set("name:"+newFileName+":id", fileUuidStr, 0)
	// change ownership
	oldParentDir, err := getEntityParentDir(fileName)
	if err != nil {
		return errors.New("unable to retrieve parent dir for old file path, that's strange: " + err.Error())
	}
	redisClient.LRem("id:"+oldParentDir.DirUUID.String()+":info:FileList", 1, fileName)
	redisClient.LPush("id:"+newParentDir.DirUUID.String()+":info:FileList", newFileName)
	return nil
}
