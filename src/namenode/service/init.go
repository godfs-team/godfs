package service

import (
	"context"
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	dnapi "gitlab.com/godfs-team/godfs/src/datanode/api"
	"google.golang.org/grpc"
	"log"
	"os"
	"strings"
)

func init() {
	fmt.Println("Loading up namenode services")
	fmt.Println("Active variables:")
	fmt.Println("  MIN_REPLICA_COUNT = ", MinimumReplicaCount)
	fmt.Println("  REDIS_ADDRESS     = ", RedisAddress)
	fmt.Println("  REDIS_PASSWORD    = ", strings.Repeat("*", len(RedisPassword)))
	fmt.Println("  REDIS_DB          = ", RedisDb)
	err := connectToRedis()
	if err != nil {
		fmt.Println("Unable to connect to the database at " + RedisAddress + ": " + err.Error())
		os.Exit(1)
	} else {
		fmt.Println("Connection to the database is established")
	}
}

func connectToRedis() (err error) {
	//fmt.Println("Connecting to redis database ", RedisDb, " on address ", RedisAddress)
	redisClient = redis.NewClient(&redis.Options{
		Addr:     RedisAddress,
		Password: RedisPassword,
		DB:       RedisDb,
	})
	_, err = redisClient.Ping().Result()
	if err != nil {
		return errors.New("unable to ping redis database of namenode: " + err.Error())
	} else {
		return nil
	}
}

func InitNamespace() error {
	// load datanodes, drop database, and update datanodes list
	datanodesInfo, err := GetAllDatanodes()
	redisClient.FlushDB()
	RunDatanodesCleanup(datanodesInfo, false)
	// create root dir
	_, err = createDir("", true)
	if err != nil {
		log.Println("Unable to create root directory in the initialized namespace")
		return errors.New("root dir init error: " + err.Error())
	}
	return nil
}

func RunDatanodesCleanup(datanodesInfo []*DatanodeInfo, deletionMode bool) {
	doRemove := func(info *DatanodeInfo) {
		if !deletionMode {
			return
		}
		err := UnregisterDatanode(info.NodeUuid)
		if err != nil {
			fmt.Println("Unable to unregisted datanode " + info.NodeUuid.String() + " while cleanup " + err.Error())
		}
	}
	for i := range datanodesInfo {
		dnInfo := datanodesInfo[i]
		datanodeAddress := dnInfo.NodePrivateAddress
		dnConn, err := grpc.Dial(datanodeAddress, grpc.WithInsecure())
		if err == nil {
			dnClient := dnapi.NewDatanodePrivateClient(dnConn)
			response, err := dnClient.Ping(context.TODO(), &dnapi.InitRequest{})
			if err == nil && response.Status == dnapi.DatanodeStatus_OK {
				if response.Msg == dnInfo.NodeUuid.String() {
					// datanode is alive, uuid is up to date, close the connection
					_ = dnConn.Close()
					if !deletionMode {
						_, err = RegisterDatanode(dnInfo)
					}
					continue
				} else {
					fmt.Println("[INIT] Datanode " + dnInfo.NodeUuid.String() + " at " + dnInfo.NodePrivateAddress + " is out of date, removing it")
					doRemove(dnInfo)
				}
			} else {
				fmt.Println("[INIT] Datanode " + dnInfo.NodeUuid.String() + " at " + dnInfo.NodePrivateAddress + " did not answer on ping, removing it")
				doRemove(dnInfo)
			}
		} else {
			fmt.Println("[INIT] Unable to connect to datanode " + dnInfo.NodeUuid.String() + " at " + dnInfo.NodePrivateAddress + ", removing it")
			doRemove(dnInfo)
		}
	}
}
