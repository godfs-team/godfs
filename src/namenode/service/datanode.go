package service

import (
	"errors"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
)

/*
 addr:<address>:<public port>:id -> uuid of node
 addr:<address>:<private port>:id -> uuid of node
 id:<uuid>:type -> type of entity (DATANODE)
 id:<uuid>:status -> current status of the datanode
 id:<uuid>:info -> string to string map to store datanode info
 list:datanodes -> list of uuid strings of datanodes
 list:files:<datanode uuid>:id -> list of uuid strings of files
*/
type DatanodeInfo struct {
	NodeUuid           uuid.UUID
	NodePublicAddress  string
	NodePrivateAddress string
}

func RegisterDatanode(info *DatanodeInfo) (datanodeUuid uuid.UUID, err error) {
	if info.NodeUuid == uuid.Nil {
		datanodeUuid = uuid.New()
	} else {
		datanodeUuid = info.NodeUuid
	}
	datanodeUuidStr := datanodeUuid.String()
	key1 := "addr:" + info.NodePrivateAddress + ":id"
	key2 := "addr:" + info.NodePublicAddress + ":id"
	redisClient.Set(key1, datanodeUuidStr, 0)
	redisClient.Set(key2, datanodeUuidStr, 0)

	redisClient.Set("id:"+datanodeUuidStr+":type", "DATANODE", 0)
	infoMap := map[string]interface{}{
		"NodeUuid":           info.NodeUuid,
		"NodePublicAddress":  info.NodePublicAddress,
		"NodePrivateAddress": info.NodePrivateAddress,
	}
	_, err = redisClient.HMSet("id:"+datanodeUuidStr+":info", infoMap).Result()
	if err != nil {
		return datanodeUuid, err
	}
	_, err = redisClient.LPush("list:datanodes", datanodeUuidStr).Result()
	if err != nil {
		return datanodeUuid, err
	}
	return datanodeUuid, nil
}

func ChangeDatanodeStatus(datanodeUuid uuid.UUID, active bool) error {
	datanodeUuidStr := datanodeUuid.String()
	typeStr, err := redisClient.Get("id:" + datanodeUuidStr + ":type").Result()
	if err == redis.Nil {
		return errors.New("unable to find type for the entity with id " + datanodeUuidStr)
	}
	if typeStr != "DATANODE" {
		return errors.New("GetDatanodeInfo can be applied for DATANODE entities only, got " + typeStr)
	}
	var stringStatus = "false"
	if active {
		stringStatus = "true"
	}
	_, err = redisClient.Set("addr:"+datanodeUuidStr+":status", stringStatus, 0).Result()
	if err != nil {
		return errors.New("unable to update datanode status: " + err.Error())
	}
	return nil
}

func UnregisterDatanode(datanodeUuid uuid.UUID) error {
	datanodeUuidStr := datanodeUuid.String()
	filesInDatanodeKey := "list:files:" + datanodeUuidStr + ":id"
	//TODO: process error
	filesInDatanode, _ := redisClient.LRange(filesInDatanodeKey, 0, -1).Result()
	for i := range filesInDatanode {
		_ = repositoryRemoveFileFromDatanode(datanodeUuidStr, filesInDatanode[i])
	}
	infoMap, _ := redisClient.HGetAll("id:" + datanodeUuidStr + ":info").Result()
	//TODO: start replication of all files from this node which should be matched with replica count
	key1 := "addr:" + infoMap["NodeAddress"] + ":id"
	key2 := "addr:" + infoMap["NodePrivateAddress"] + ":id"
	key3 := "id:" + datanodeUuidStr + ":type"
	key4 := "id:" + datanodeUuidStr + ":info"
	redisClient.Del(key1, key2, key3, key4)
	redisClient.LRem("list:datanodes", 1, datanodeUuidStr)
	return nil
}

func GetAllDatanodes() (datanodes []*DatanodeInfo, err error) {
	datanodesUuids, err := redisClient.LRange("list:datanodes", 0, -1).Result()
	if err != nil {
		return nil, err
	}
	datanodesInfoList := make([]*DatanodeInfo, 0, len(datanodesUuids))
	for i := 0; i < len(datanodesUuids); i++ {
		datanodeUuid := datanodesUuids[i]
		info, err := GetDatanodeInfo(datanodeUuid)
		if err != nil {
			errStr := "unable to retrieve datanode info for uuid "
			return nil, errors.New(errStr + datanodeUuid + ": " + err.Error())
		}
		datanodesInfoList = append(datanodesInfoList, info)
	}
	//log.Printf("GetAllDatanodes: %v datanodes\n", len(datanodesInfoList))
	return datanodesInfoList, nil
}

func GetDatanodeInfoByUuid(uuid uuid.UUID) (info *DatanodeInfo, err error) {
	uuidStr := uuid.String()
	return GetDatanodeInfo(uuidStr)
}

func GetDatanodeInfo(uuidStr string) (info *DatanodeInfo, err error) {
	typeStr, err := redisClient.Get("id:" + uuidStr + ":type").Result()
	if err == redis.Nil {
		return nil, errors.New("unable to find type for the entity with id " + uuidStr)
	}
	if typeStr != "DATANODE" {
		return nil, errors.New("GetDatanodeInfo can be applied for DATANODE entities only, got " + typeStr)
	}
	infoMap, err := redisClient.HGetAll("id:" + uuidStr + ":info").Result()
	if err == redis.Nil {
		return nil, errors.New("unable to find info for DATANODE " + uuidStr)
	}
	datanodeUuid, _ := uuid.Parse(uuidStr)
	return &DatanodeInfo{
		NodeUuid:           datanodeUuid,
		NodePublicAddress:  infoMap["NodePublicAddress"],
		NodePrivateAddress: infoMap["NodePrivateAddress"],
	}, nil
}

func repositoryAddFileToDatanode(datanodeUuid string, fileUuid string) (err error) {
	filesInDatanodeKey := "list:files:" + datanodeUuid + ":id"
	datanodesForFileKey := "id:" + fileUuid + ":info:StorageUUIDs"
	redisClient.LPush(filesInDatanodeKey, fileUuid)
	redisClient.LPush(datanodesForFileKey, datanodeUuid)
	return nil
}

func repositoryRemoveFileFromDatanode(datanodeUuid string, fileUuid string) (err error) {
	filesInDatanodeKey := "list:files:" + datanodeUuid + ":id"
	datanodesForFileKey := "id:" + fileUuid + ":info:StorageUUIDs"
	_, err = redisClient.LRem(filesInDatanodeKey, 1, fileUuid).Result()
	if err != nil {
		return err
	}
	_, err = redisClient.LRem(datanodesForFileKey, 1, datanodeUuid).Result()
	if err != nil {
		return err
	}
	return nil
}
