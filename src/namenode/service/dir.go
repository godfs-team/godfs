package service

import (
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
	"strings"
)

/**
For each DirInfo

name:<dirName>:id -> uuid string
id:<uuid>:type -> type string (DIR)
id:<uuid>:info -> hash map{ FullName }
id:<uuid>:info:FileList -> string list
id:<uuid>:info:SubDirList -> string list
*/

type DirInfo struct {
	// full path of the directory
	FullName string
	// uuid of the directory (may be unused)
	DirUUID    uuid.UUID
	SubDirList []string
	// list of file names whose are located in this directory
	FileList []string
}

func GetDirInfo(dirName string) (dirInfo DirInfo, err error) {
	// find directory info in namenode database
	uuidStr, err := redisClient.Get("name:" + dirName + ":id").Result()
	if err == redis.Nil {
		return DirInfo{}, errors.New("dir name " + dirName + " is not found in the namespace")
	}

	dirUuid, err := uuid.Parse(uuidStr)
	if err != nil {
		panic("name:" + dirName + ":id stores incorrect value " + uuidStr + " instead of uuid")
	}
	return GetDirInfoByUuid(dirUuid)
}

func GetDirInfoByUuid(dirUuid uuid.UUID) (dirInfo DirInfo, err error) {
	dirUuidStr := dirUuid.String()
	typeStr, err := redisClient.Get("id:" + dirUuid.String() + ":type").Result()
	if err == redis.Nil {
		return DirInfo{}, errors.New("type for uuid " + dirUuidStr + " is not found in the namespace")
	} else if typeStr != "DIR" {
		return DirInfo{}, errors.New("GetDirInfo can not be applied to entity of type " + typeStr)
	}

	infoBaseStr := "id:" + dirUuidStr + ":info"
	infoMap, err := redisClient.HGetAll(infoBaseStr).Result()
	if err == redis.Nil {
		return DirInfo{}, errors.New("unable to find info for DIR " + dirUuidStr)
	}

	dirInfo.FullName = infoMap["FullName"]
	dirInfo.DirUUID = dirUuid

	fileList, err := redisClient.LRange(infoBaseStr+":FileList", 0, -1).Result()
	if err == redis.Nil {
		// list is not found due to no files inside the DIR
		fileList = make([]string, 0)
		//return DirInfo{}, errors.New("unable to find FileList for DIR " + dirUuidStr)
	}
	dirInfo.FileList = fileList
	subDirList, err := redisClient.LRange(infoBaseStr+":SubDirList", 0, -1).Result()
	if err == redis.Nil {
		// list in not found due to no subdirs inside the DIR
		subDirList = make([]string, 0)
		//return DirInfo{}, errors.New("unable to find SubDirList for DIR " + dirUuidStr)
	}
	dirInfo.SubDirList = subDirList
	return dirInfo, nil
}

func CreateDir(dirName string) (dirInfo DirInfo, err error) {
	return createDir(dirName, false)
}

func createDir(dirName string, isRoot bool) (dirInfo DirInfo, err error) {
	// put new record about the directory into namenode database
	dirUuid := uuid.New()
	dirUuidStr := dirUuid.String()
	dirInfo.FullName = dirName
	dirInfo.DirUUID = dirUuid
	dirInfo.SubDirList = make([]string, 0)
	dirInfo.FileList = make([]string, 0)

	infoBaseStr := "id:" + dirUuidStr + ":info"
	infoMap := map[string]interface{}{
		"FullName": dirName,
	}
	// add dir to the parent dir
	if !isRoot {
		subDirInfo, err := getEntityParentDir(dirName)
		if err != nil {
			fmt.Println("Unable to find parent DIR for the creating DIR", dirName, ": ", err)
			return dirInfo, err
		}
		_, err = redisClient.LPush("id:"+subDirInfo.DirUUID.String()+":info:SubDirList", dirName).Result()
		if err != nil {
			fmt.Println("Unable to add creating DIR to the parent DIR", subDirInfo.FullName, ": ", err)
			return dirInfo, err
		}
	}
	// TODO: DONE process error if we will have to
	_, err = redisClient.Set("name:"+dirName+":id", dirUuidStr, 0).Result()
	if err != nil {
		fmt.Printf("Error (Redis) on CreateDir: %v", err)
		return dirInfo, err
	}
	_, err = redisClient.Set("id:"+dirUuidStr+":type", "DIR", 0).Result()
	if err != nil {
		fmt.Printf("Error (Redis) on CreateDir: %v", err)
		return dirInfo, err
	}
	_, err = redisClient.HMSet(infoBaseStr, infoMap).Result()
	if err != nil {
		fmt.Printf("Error (Redis) on CreateDir: %v", err)
		return dirInfo, err
	}

	// empty lists are not supported by redis
	/*_, err = redisClient.LPush(infoBaseStr + ":FileList").Result()
	if err!=nil{
		fmt.Printf("Error (Redis) on CreateDir: %v",err)
		return dirInfo, err
	}
	_, err = redisClient.LPush(infoBaseStr + ":SubDirList").Result()
	if err!=nil{
		fmt.Printf("Error (Redis) on CreateDir: %v",err)
		return dirInfo, err
	}*/
	return dirInfo, nil
}

func RemoveDir(dirName string, withForce bool) error {
	// if withForce flag set to false and there is any objects inside the directory, return error
	// otherwise recursively remove all folders from namenode and files from both namenode and datanode
	// cleanup dir record in local database
	dirInfo, err := GetDirInfo(dirName)
	if err != nil {
		return errors.New("nested error: " + err.Error())
	}
	dirUuidStr := dirInfo.DirUUID.String()
	infoBaseStr := "id:" + dirUuidStr + ":info"

	if !withForce && (len(dirInfo.SubDirList) != 0 || len(dirInfo.FileList) != 0) {
		return errors.New("directory not empty, use force")
	}
	// check sub dirs, run recursive remove
	for i := range dirInfo.SubDirList {
		subDirName := dirInfo.SubDirList[i]
		err := RemoveDir(subDirName, withForce)
		if err != nil {
			return errors.New("nested dir " + subDirName + " removal failed: " + err.Error())
		}
	}
	// check files, run their removal
	for i := range dirInfo.FileList {
		fileName := dirInfo.FileList[i]
		err := RemoveFile(fileName)
		if err != nil {
			return errors.New("file " + fileName + " removal failed: " + err.Error())
		}
	}

	//at this point it is guaranteed, that dir have no children entity
	keysToRemove := []string{
		"name:" + dirName + ":id",
		"id:" + dirUuidStr + ":type",
		infoBaseStr,
		infoBaseStr + ":FileList",
		infoBaseStr + ":SubDirList",
	}
	_, _ = redisClient.Del(keysToRemove...).Result()
	// remove directory from list of parent entity
	parentDirInfo, err := getEntityParentDir(dirName)
	if err != nil {
		return errors.New("parent dir retrieval error: " + err.Error())
	}
	redisClient.LRem("id:"+parentDirInfo.DirUUID.String()+":info:SubDirList", 1, dirName)
	return nil
}

func getEntityParentDir(entityFullName string) (dirInfo DirInfo, err error) {
	ind := strings.LastIndex(entityFullName, "/")
	return GetDirInfo(entityFullName[:ind])
}
