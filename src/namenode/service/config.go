package service

import (
	"github.com/go-redis/redis"
	"os"
	"strconv"
)

/* vars */
/*
dir name -> dir uuid
dir uuid -> dir info
file name -> file uuid
file uuid -> file info
*/
var redisClient *redis.Client

/* consts */

const defaultMinimumReplicaCount = 2
const defaultRedisAddress = "redis:6379"
const defaultRedisPassword = ""
const defaultRedisDB = 0

var MinimumReplicaCount = func() int {
	valStr, found := os.LookupEnv("MIN_REPLICA_COUNT")
	var val int
	if !found {
		val = defaultMinimumReplicaCount
	} else {
		var err error
		val, err = strconv.Atoi(valStr)
		if err != nil {
			println("Unable to parse MIN_REPLICA_COUNT: ", err)
			println("Falling back to default value ", defaultMinimumReplicaCount)
			val = defaultMinimumReplicaCount
		}
	}
	return val
}()

var RedisAddress = func() string {
	val, found := os.LookupEnv("REDIS_ADDRESS")
	if !found {
		val = defaultRedisAddress
	}
	return val
}()

var RedisPassword = func() string {
	val, found := os.LookupEnv("REDIS_PASSWORD")
	if !found {
		val = defaultRedisPassword
	}
	return val
}()

var RedisDb = func() int {
	valStr, found := os.LookupEnv("REDIS_DB")
	var val int
	if !found {
		val = defaultRedisDB
	} else {
		var err error
		val, err = strconv.Atoi(valStr)
		if err != nil {
			println("Unable to parse REDIS_DB: ", err)
			println("Falling back to default value ", defaultRedisDB)
			val = defaultRedisDB
		}
	}
	return val
}()
