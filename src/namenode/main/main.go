package main

import (
	"fmt"
	grpc_server "gitlab.com/godfs-team/godfs/src/namenode/grpc-server"
	"google.golang.org/grpc"
	"log"
)

func main() {
	fmt.Println("Loading up namenode servers")
	 runPingWorker := func() {
		err := grpc_server.RunPingWorker()
		if err != nil {
			log.Fatalln("PingWorker run error:", err)
		}
	}
	runPublicFunc := func() {
		s := grpc_server.PublicGRPCServer{}
		srv := grpc.NewServer()
		err := s.RunServer(srv)
		if err != nil {
			log.Fatalln("PublicServer run error:", err)
		}
	}
	runPrivateFunc := func() {
		s := grpc_server.PrivateGRPCServer{}
		srv := grpc.NewServer()
		err := s.RunServer(srv)
		if err != nil {
			log.Fatalln("PrivateServer run error:", err)
		}
	}
	go runPingWorker()
	go runPrivateFunc()
	runPublicFunc()
}
