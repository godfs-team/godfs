package grpc_server

import (
	"context"
	"fmt"
	dnapi "gitlab.com/godfs-team/godfs/src/datanode/api"
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	"gitlab.com/godfs-team/godfs/src/namenode/api"
	"gitlab.com/godfs-team/godfs/src/namenode/service"
	"google.golang.org/grpc"
	"log"
	"net"
	"strconv"
)

// PublicGRPCServer --
type PublicGRPCServer struct{}

func (s *PublicGRPCServer) RunServer(srv *grpc.Server) (err error) {
	api.RegisterNamenodeServer(srv, s)

	var listenAddress = PublicAddress
	listenAddress = "0.0.0.0:" + utils.GetPortString(listenAddress)
	l, err := net.Listen("tcp", listenAddress)
	if err != nil {
		return err
	}
	err = srv.Serve(l)
	if err != nil {
		return err
	}
	return nil
}

func (s *PublicGRPCServer) Init(context.Context, *api.InitRequest) (*api.Response, error) {
	datanodes, err := service.GetAllDatanodes()
	if err != nil {
		fmt.Printf("REQ Init[PUB] failed [GetAlldatanodes] : %v", err)
		return &api.Response{
			Status: api.Status_ERROR,
			Msg:    err.Error(),
		}, err
	}
	for i := 0; i < len(datanodes); i++ {
		datanodeAddress := datanodes[i].NodePrivateAddress
		dnConn, err := grpc.Dial(datanodeAddress, grpc.WithInsecure())
		defer func() {
			_ = dnConn.Close()
		}()
		if err != nil {
			log.Println("Datanode ", datanodes[i].NodeUuid.String(), " at ", datanodeAddress, " did not accepted connection")
			return &api.Response{
				Status: api.Status_ERROR,
				Msg:    err.Error(),
			}, err
		}
		dnClient := dnapi.NewDatanodePrivateClient(dnConn)
		response, err := dnClient.InitStorage(context.TODO(), &dnapi.InitRequest{})
		if err != nil {
			log.Println("Datanode ", datanodes[i].NodeUuid.String(), " at ", datanodeAddress, " did not accepted connection")
			return &api.Response{
				Status: api.Status_ERROR,
				Msg:    err.Error(),
			}, err
		}
		if response.Status != dnapi.DatanodeStatus_OK {
			log.Println("Datanode ", datanodes[i].NodeUuid.String(), " at ", datanodeAddress, " did not accepted connection")
			return &api.Response{
				Status: api.Status_ERROR,
				Msg:    response.Msg,
			}, err
		}
	}

	err = service.InitNamespace()
	if err != nil {
		fmt.Println("REQ InitNamespace[PUB] failed (parsing): ", err)
		return &api.Response{
			Status: api.Status_ERROR,
			Msg:    err.Error(),
		}, nil
	} else {
		fmt.Println("REQ InitNamespace[PUB] executed")
		return &api.Response{
			Status: api.Status_OK,
			Msg:    "done",
		}, err
	}
}

func (s *PublicGRPCServer) ChangeNamespaceEntity(ctx context.Context, request *api.NSChangeRequest) (*api.Response, error) {
	var err error
	switch request.Operation {
	case api.ChangeOperationType_ADD:
		{
			if len(request.Subjects) != 1 {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->ADD failed (subject count mismatch): exp 1, got ", len(request.Subjects))
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    "ADD operation requires exactly 1 subject",
				}, nil
			}
			if request.Subjects[0].Type == api.NamespaceEntityType_FILE {
				_, err = service.CreateFile(request.Subjects[0].Name)
			} else {
				_, err = service.CreateDir(request.Subjects[0].Name)
			}
			if err != nil {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->ADD failed: ", err)
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    "ADD operation failed: " + err.Error(),
				}, nil
			} else {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->ADD executed: ", request.Subjects[0].Name, "of type", request.Subjects[0].Type.String())
				return &api.Response{
					Status: api.Status_OK,
					Msg:    "ADD operation executed: entity " + request.Subjects[0].Name + " has been created",
				}, nil
			}
		}
	case api.ChangeOperationType_REMOVE:
		{
			if len(request.Subjects) != 1 {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->REMOVE failed (subject count mismatch): exp 1, got ", len(request.Subjects))
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    "REMOVE operation requires exactly 1 subject",
				}, nil
			}
			if request.Subjects[0].Type == api.NamespaceEntityType_FILE {
				err = service.RemoveFile(request.Subjects[0].Name)
			} else {
				err = service.RemoveDir(request.Subjects[0].Name, request.WithForce)
			}
			if err != nil {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->REMOVE failed: ", err)
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    "REMOVE operation failed: " + err.Error(),
				}, nil
			} else {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->REMOVE executed")
				return &api.Response{
					Status: api.Status_OK,
					Msg:    "REMOVE operation executed: entity " + request.Subjects[0].Name + " has been deleted",
				}, nil
			}
		}
	case api.ChangeOperationType_MOVE:
		fallthrough
	case api.ChangeOperationType_COPY:
		{
			var operationName string
			if request.Operation == api.ChangeOperationType_MOVE {
				operationName = "MOVE"
			} else {
				operationName = "COPY"
			}
			if len(request.Subjects) != 2 {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->"+operationName+" failed (subject count mismatch): exp 2, got ", len(request.Subjects))
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    operationName + " operation requires exactly 2 subjects",
				}, nil
			}
			if request.Subjects[0].Type != api.NamespaceEntityType_FILE ||
				request.Subjects[1].Type != api.NamespaceEntityType_FILE {
				errStr := "REQ ChangeNamespaceEntity[PUB]->" + operationName + " failed (entity type error): exp (FILE, FILE), got "
				fmt.Println(errStr + " (" + request.Subjects[0].Type.String() + ", " + request.Subjects[1].Type.String() + ")")
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    operationName + " operation can be performed on FILE entities only",
				}, nil
			}
			var err error
			subjects := request.Subjects
			if request.Operation == api.ChangeOperationType_COPY {
				err = service.CopyFile(subjects[0].Name, subjects[1].Name)
			} else {
				err = service.MoveFile(subjects[0].Name, subjects[1].Name)
			}
			if err != nil {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->"+operationName+" failed: ", err)
				return &api.Response{
					Status: api.Status_ERROR,
					Msg:    operationName + " operation failed: " + err.Error(),
				}, nil
			} else {
				fmt.Println("REQ ChangeNamespaceEntity[PUB]->" + operationName + " executed")
				return &api.Response{
					Status: api.Status_OK,
					Msg:    operationName + " operation executed",
				}, nil
			}
		}
	}
	fmt.Println("REQ ChangeNamespaceEntity[PUB]->????? (unrecognized operation): " + request.Operation.String())
	return &api.Response{
		Status: api.Status_ERROR,
		Msg:    "Unrecognized operation " + strconv.Itoa(int(request.Operation)),
	}, nil
}

func (s *PublicGRPCServer) NamespaceEntityInfo(ctx context.Context, entity *api.NSEntity) (*api.EntityInfoResponse, error) {
	var err error
	if entity.Type == api.NamespaceEntityType_FILE {
		var fileInfo service.FileInfo
		fileInfo, err = service.GetFileInfo(entity.Name)
		if err == nil {
			dataNodesList := make([]*api.DataNodeInfo, len(fileInfo.StorageUUIDs))
			for i := 0; i < len(fileInfo.StorageUUIDs); i++ {
				dataNodeUuid := fileInfo.StorageUUIDs[i]
				var address string
				datanodeInfo, err := service.GetDatanodeInfoByUuid(dataNodeUuid)
				if err != nil {
					fmt.Println("WARN NamespaceEntityInfo: unable to get datanode info for uuid " + dataNodeUuid.String())
					address = "error"
				} else {
					address = datanodeInfo.NodePublicAddress
				}
				dataNodesList[i] = &api.DataNodeInfo{
					ServerUuid:    dataNodeUuid.String(),
					ServerAddress: address,
				}
			}
			fmt.Println("REQ NamespaceEntityInfo[PUB]->FILE executed:", fileInfo.FullName, "=", fileInfo.FileUUID)
			return &api.EntityInfoResponse{
				Status: api.Status_OK,
				Msg:    "FILE info was successfully retrieved",
				Type:   api.NamespaceEntityType_FILE,
				FileInfo: &api.FileInfo{
					FileUUID:  fileInfo.FileUUID.String(),
					FileSize:  fileInfo.Size,
					DataNodes: dataNodesList,
				},
			}, nil
		}
	} else {
		var dirInfo service.DirInfo
		dirInfo, err = service.GetDirInfo(entity.Name)
		if err == nil {
			fmt.Println("REQ NamespaceEntityInfo[PUB]->DIR executed: ", dirInfo.FullName, "=", dirInfo.DirUUID, ", ", len(dirInfo.SubDirList), " subdirs, ", len(dirInfo.FileList), " files")
			return &api.EntityInfoResponse{
				Status: api.Status_OK,
				Msg:    "DIR info was successfully retrieved",
				Type:   api.NamespaceEntityType_DIR,
				DirInfo: &api.DirInfo{
					Subdirs: dirInfo.SubDirList,
					Files:   dirInfo.FileList,
				},
			}, nil
		}
	}
	fmt.Println("REQ NamespaceEntityInfo[PUB] failed: " + err.Error())
	return &api.EntityInfoResponse{
		Status: api.Status_ERROR,
		Msg:    "Entity info retrieving failed: " + err.Error(),
	}, nil
}
