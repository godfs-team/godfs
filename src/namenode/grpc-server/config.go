/**
GRPC server configuration
*/
package grpc_server

import (
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	"log"
	"os"
	"time"
)

const defaultPublicAddress = "0.0.0.0:9090"
const defaultPrivateAddress = "0.0.0.0:9091"

var PingInterval = time.Second * 5

var PublicAddress = func() string {
	var err error
	addr, passed := os.LookupEnv("PUBLIC_ADDRESS")
	if !passed {
		addr = defaultPublicAddress
	} else {
		addr, err = utils.ProcessAddressTemplate(addr)
		if err != nil {
			log.Fatalln("Unable to retrieve public address of the node: ", err)
		}
	}
	return addr
}()

var PrivateAddress = func() string {
	addr, passed := os.LookupEnv("PRIVATE_ADDRESS")
	if !passed {
		addr = defaultPrivateAddress
	}
	return addr
}()
