package grpc_server

import (
	"context"
	dnapi "gitlab.com/godfs-team/godfs/src/datanode/api"
	"gitlab.com/godfs-team/godfs/src/namenode/service"
	"google.golang.org/grpc"
	"log"
	"time"
)

func RunPingWorker() (err error) {
	//cleanedUp := false
	for {
		//if !cleanedUp {
		//	time.Sleep(time.Second * 3)
		//	datanodes, err := service.GetAllDatanodes()
		//	if err != nil {
		//		log.Println("Unable to run pre-ping cleanup: " + err.Error())
		//	} else {
		//		service.RunDatanodesCleanup(datanodes, true)
		//		cleanedUp = true
		//	}
		//}
		time.Sleep(PingInterval)
		datanodes, err := service.GetAllDatanodes()
		if err != nil {
			log.Println("Unable to get list of all datanodes to ping:", err)
			continue
		}
		//log.Println("Checking datanodes to be alive:",len(datanodes),"expected")
		for i := 0; i < len(datanodes); i++ {
			datanodeAddress := datanodes[i].NodePrivateAddress
			dnConn, err := grpc.Dial(datanodeAddress, grpc.WithInsecure())
			if err == nil {
				dnClient := dnapi.NewDatanodePrivateClient(dnConn)
				response, err := dnClient.Ping(context.TODO(), &dnapi.InitRequest{})
				if err == nil && response.Status == dnapi.DatanodeStatus_OK {
					// datanode is alive, close the connection
					_ = dnConn.Close()
					continue
				}
			}
			_ = runBackupReplication(datanodes[i])
		}
	}
}

func runBackupReplication(datanodeInfo *service.DatanodeInfo) (err error) {
	log.Println("Datanode ", datanodeInfo.NodeUuid.String(), " at ", datanodeInfo.NodePrivateAddress, " did not accept connection")
	//TODO: start another goroutine
	err = service.BeginDatanodeReplication(datanodeInfo.NodeUuid)
	if err != nil {
		log.Println("Replication of datanode ", datanodeInfo.NodeUuid, "failed:", err)
		return err
	}
	//TODO: unregister it
	err = service.UnregisterDatanode(datanodeInfo.NodeUuid)
	if err != nil {
		log.Println("Unable to unregister dead datanode ", datanodeInfo.NodeUuid, ": ", err)
		return err
	}
	return nil
}
