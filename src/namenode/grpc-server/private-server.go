package grpc_server

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"gitlab.com/godfs-team/godfs/src/datanode/utils"
	"gitlab.com/godfs-team/godfs/src/namenode/api"
	"gitlab.com/godfs-team/godfs/src/namenode/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/peer"
	"net"
	"strings"
)

type PrivateGRPCServer struct{}

func (s *PrivateGRPCServer) RunServer(srv *grpc.Server) (err error) {
	api.RegisterNamenodePrivateServer(srv, s)

	var listenAddress = PrivateAddress
	listenAddress = "0.0.0.0:" + utils.GetPortString(listenAddress)
	l, err := net.Listen("tcp", listenAddress)
	if err != nil {
		return err
	}
	err = srv.Serve(l)
	if err != nil {
		return err
	}
	return nil
}

func (s *PrivateGRPCServer) GetFileInfo(ctx context.Context, request *api.FileInfoRequest) (response *api.FileInfoResponse, err error) {
	fileUuidStr := request.FileUuid
	fileUuid, err := uuid.Parse(fileUuidStr)
	if err != nil {
		fmt.Println("REQ GetFileInfo[PRIV] failed (parsing): ", err)
		return &api.FileInfoResponse{
			Status: api.Status_ERROR,
			Msg:    "invalid file uuid: " + err.Error(),
			Info:   nil,
		}, nil
	}
	info, err := service.GetFileInfoByUuid(fileUuid)
	if err != nil {
		fmt.Println("REQ GetFileInfo[PRIV] failed (info by uuid): ", err)
		return &api.FileInfoResponse{
			Status: api.Status_ERROR,
			Msg:    "nested error: " + err.Error(),
			Info:   nil,
		}, nil
	}
	dataNodesList := make([]*api.DataNodeInfo, len(info.StorageUUIDs))
	for i := 0; i < len(info.StorageUUIDs); i++ {
		dataNodeUuid := info.StorageUUIDs[i]
		var address string
		datanodeInfo, err := service.GetDatanodeInfoByUuid(dataNodeUuid)
		if err != nil {
			println("WARN: enable to get datanode info for uuid " + dataNodeUuid.String())
			address = "error"
		} else {
			address = datanodeInfo.NodePrivateAddress
		}
		dataNodesList[i] = &api.DataNodeInfo{
			ServerUuid:    dataNodeUuid.String(),
			ServerAddress: address,
		}
	}
	fmt.Println("REQ GetFileInfo[PRIV] executed")
	return &api.FileInfoResponse{
		Status: api.Status_OK,
		Msg:    "ok",
		Info: &api.FileInfo{
			FileUUID:  fileUuidStr,
			FileSize:  info.Size,
			DataNodes: dataNodesList,
		},
	}, nil
}

func (s *PrivateGRPCServer) RegisterDatanode(ctx context.Context, request *api.DatanodeRegisterRequest) (*api.DatanodeRegisterResponse, error) {
	if strings.HasPrefix(request.PrivateAddress, "0.0.0.0:") {
		fmt.Println("Datanode reqistered with 0.0.0.0 private address, using endpoint resolving")
		datanodePeer, ok := peer.FromContext(ctx)
		if ok {
			datanodePrivatePortStr := utils.GetPortString(request.PrivateAddress)
			request.PrivateAddress = strings.Split(datanodePeer.Addr.String(), ":")[0] + ":" + datanodePrivatePortStr
		}
	}
	datanodeUuid, err := service.RegisterDatanode(&service.DatanodeInfo{
		NodeUuid:           uuid.Nil,
		NodePublicAddress:  request.PublicAddress,
		NodePrivateAddress: request.PrivateAddress,
	})
	if err != nil {
		fmt.Println("REQ RegisterDatanode[PRIV] failed: ", err)
		return &api.DatanodeRegisterResponse{
			Status: api.Status_ERROR,
			Msg:    err.Error(),
		}, nil
	}
	fmt.Println("REQ RegisterDatanode[PRIV] executed")
	fmt.Println("NodePublicAddress:", request.PublicAddress)
	fmt.Println("NodePrivateAddress:", request.PrivateAddress)
	return &api.DatanodeRegisterResponse{
		Status:         api.Status_OK,
		Msg:            "ok",
		GeneratedUuid:  datanodeUuid.String(),
		PrivateAddress: request.PrivateAddress,
	}, nil
}

func (s *PrivateGRPCServer) UnregisterDatanode(cdx context.Context, request *api.DatanodeUnregisterRequest) (*api.Response, error) {
	datanodeUuid, err := uuid.Parse(request.DatanodeUuid)
	if err != nil {
		fmt.Println("REQ UnregisterDatanode[PRIV] failed (parsing): ", err)
		return &api.Response{
			Status: api.Status_ERROR,
			Msg:    "unable to parse provided DATANODE uuid:" + err.Error(),
		}, nil
	}
	err = service.UnregisterDatanode(datanodeUuid)
	if err != nil {
		fmt.Println("REQ UnregisterDatanode[PRIV] failed: ", err)
		return &api.Response{
			Status: api.Status_ERROR,
			Msg:    err.Error(),
		}, nil
	} else {
		fmt.Println("REQ UnregisterDatanode[PRIV] executed")
		return &api.Response{
			Status: api.Status_OK,
			Msg:    "ok",
		}, nil
	}
}
