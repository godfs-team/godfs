#!/usr/bin/env bash

build(){
local DOCKER_DIR=docker
local COMPONENT=base
local TAG=local
local DOCKER_REGISTRY=registry.gitlab.com/godfs-team/godfs/${COMPONENT}
local DOCKER_PATH=${DOCKER_DIR}/${COMPONENT}/Dockerfile
local CONTEXT=.

docker build -t ${DOCKER_REGISTRY}:${TAG} -f ${DOCKER_PATH} ${CONTEXT}
}
build
